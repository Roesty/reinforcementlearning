/*
 * Entity.cpp
 *
 *  Created on: Mar 21, 2022
 *      Author: mooseman
 */

#include "Entity.h"

void Entity::draw(Shader shader)
{
	if (m_draw && m_mesh.get())
	{
		m_mesh->draw(m_physical->m_body, shader, m_debugAngle);
	}
}

Entity::Entity()
{
	m_debugAngle=0.0f;
	m_draw = true;
}

Entity::~Entity()
{
	// TODO Auto-generated destructor stub
}

float Entity::randFloat()
{
	return float(rand())/float(RAND_MAX);
}

float Entity::getMass()
{
	return m_physical->m_body->GetMass();
}

b2Vec2 Entity::getVelocity()
{
	return m_physical->m_body->GetLinearVelocity();
}

bool Entity::getAwake()
{
	return m_physical->m_body->IsAwake();
}

void Entity::setMidValue(int layer, int slot, float value)
{
	if (m_neuralNetwork.get())
	{
		m_neuralNetwork->m_middle(slot) = value;
	}
}

void Entity::setPosition(const glm::vec3& mPosition)
{
	if (m_physical.get())
	{
		m_physical->setPosition(mPosition);
	}
}

void Entity::setInValue(int slot, float value, bool activate)
{
	if (m_neuralNetwork.get())
	{
		if (activate)
		{
			m_neuralNetwork->m_in(slot) = m_neuralNetwork->activate(value);
		}
		else
		{
			m_neuralNetwork->m_in(slot) = value;
		}
	}
}

float Entity::getOutValue(int slot)
{
	if (m_neuralNetwork.get())
	{
		return m_neuralNetwork->m_out(slot);
	}
}

float Entity::getInValue(int slot)
{
	if (m_neuralNetwork.get())
	{
		return m_neuralNetwork->m_in(slot);
	}
}

void Entity::setVelocity(b2Vec2 velocity)
{
	m_physical->m_body->SetLinearVelocity(velocity);
}

void Entity::setVelocity(glm::vec2 velocity)
{
	setVelocity(b2Vec2(velocity.x, velocity.y));
}

void Entity::setVelocity(glm::vec3 velocity)
{
	setVelocity(b2Vec2(velocity.x, velocity.y));
}

void Entity::setVelocity(float x, float y)
{
	setVelocity(b2Vec2(x, y));
}

void Entity::setAngle(float a)
{
	m_physical->m_body->SetTransform(m_physical->m_body->GetPosition(), a);
}
