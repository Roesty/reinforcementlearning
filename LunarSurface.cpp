/*
 * LunarSurface.cpp
 *
 *  Created on: Mar 21, 2022
 *      Author: mooseman
 */

#include "LunarSurface.h"

LunarSurface::LunarSurface(b2World* world)
{
	m_width = 200.0f;

    m_startPlatformWidth = 30.0f;
    m_targetPlatformWidth = 30.0f;

    m_startSensorWidth = 15.0f;
    m_targetSensorWidth = 15.0f;
    m_sensorHeight = 0.2f;

    m_startPlatformPosition.x = -70.0f;
    m_startPlatformPosition.y = 20.0f;

    m_targetPlatformPosition.x = 70.0f;
    m_targetPlatformPosition.y = 20.0f;

//    m_sensorShapes.clear();

//	m_startSensorName = "startSensor";
//	m_targetSensorName = "targetSensor";
//	m_badSensorName = "badSensor";

//	m_mesh.reset(new Mesh);
//	m_mesh->setToBoardRoof3(1.0f, 480, 64);

	construct(world);

//	m_physical.reset(new Physical(world, &bodyDef, &m_mesh->m_physicalVertices));
//	m_physical.reset(new Physical(world, &bodyDef, m_mesh.get()));
}

void LunarSurface::act(Scene* scene)
{
}

void LunarSurface::construct(b2World* world)
{
	m_bodyDefinitions.push_back(b2BodyDef());
	m_bodyDefinitions.back().type = b2_staticBody;

	m_fixtureDefinitions.push_back(b2FixtureDef());
	m_fixtureDefinitions.back().restitution = 0.1f;
	m_fixtureDefinitions.back().friction = 0.8f;
	m_fixtureDefinitions.back().density = 10.0f;

	m_fixtureDefinitions.push_back(b2FixtureDef());
	m_fixtureDefinitions.back().isSensor = true;

	//Create the surface
	float startPlatformLeft = m_startPlatformPosition.x - (m_startPlatformWidth/2.0f);
	float startPlatformRight = m_startPlatformPosition.x + (m_startPlatformWidth/2.0f);

	float targetPlatformLeft = m_targetPlatformPosition.x - (m_targetPlatformWidth/2.0f);
	float targetPlatformRight = m_targetPlatformPosition.x + (m_targetPlatformWidth/2.0f);

	float bottomPosition = -100.0f;

	float halfWidth = m_width / 2.0f;
	float xProgress = -halfWidth;

	while (xProgress < halfWidth)
	{
		float yPosition = randFloat()*5.0f;
		float xPosition = xProgress;

		if (xPosition >= startPlatformLeft && xPosition < startPlatformRight)
		{
			xPosition = startPlatformLeft;
			m_surface.push_back({xPosition, yPosition});

			xPosition = startPlatformLeft;
			yPosition = m_startPlatformPosition.y;
			m_surface.push_back({xPosition, yPosition});

			xPosition = startPlatformRight;
			m_surface.push_back({xPosition, yPosition});

			xProgress = startPlatformRight;
		}
		else if (xPosition >= targetPlatformLeft && xPosition < targetPlatformRight)
		{
			xPosition = targetPlatformLeft;
			m_surface.push_back({xPosition, yPosition});

			xPosition = targetPlatformLeft;
			yPosition = m_targetPlatformPosition.y;
			m_surface.push_back({xPosition, yPosition});

			xPosition = targetPlatformRight;
			m_surface.push_back({xPosition, yPosition});

			xProgress = targetPlatformRight;
		}
		else
		{
			m_surface.push_back({xPosition, yPosition});
			xProgress += randFloat()*10.0f+2.0f;

			if (xProgress >= halfWidth)
			{
				m_surface.push_back({halfWidth, yPosition});
			}
		}
	}

	//With the path created, place two more points to represent the bottom corners
	m_surface.push_back({halfWidth, bottomPosition});
	m_surface.push_back({-halfWidth, bottomPosition});

//	std::cout<<"First point: "<<m_surface[0].x<<", "<<m_surface[0].y<<std::endl;

//	std::cout<<"Surface size: "<<m_surface.size()<<" vertices"<<std::endl;

	//Create a loop with these
	m_chainShapes.push_back(std::shared_ptr<b2ChainShape>(new b2ChainShape));
	m_chainShapes.back()->CreateChain(m_surface.data(), m_surface.size());

	//Place the sensors
	float startSensorNearLeft = m_startPlatformPosition.x - (m_startSensorWidth/2.0f);
	float startSensorNearRight = m_startPlatformPosition.x + (m_startSensorWidth/2.0f);

	float startSensorFarLeft = m_startPlatformPosition.x - (m_startPlatformWidth/2.0f);
	float startSensorFarRight = m_startPlatformPosition.x + (m_startPlatformWidth/2.0f);

	float targetSensorNearLeft = m_targetPlatformPosition.x - (m_targetSensorWidth/2.0f);
	float targetSensorNearRight = m_targetPlatformPosition.x + (m_targetSensorWidth/2.0f);

	float targetSensorFarLeft = m_targetPlatformPosition.x - (m_targetPlatformWidth/2.0f);
	float targetSensorFarRight = m_targetPlatformPosition.x + (m_targetPlatformWidth/2.0f);

	m_sensorShapes = std::vector<b2Shape*>(MAX_SENSOR_SHAPES);

	std::vector<b2Vec2> startSensor;
	startSensor.push_back({startSensorNearLeft, m_startPlatformPosition.y});
	startSensor.push_back({startSensorNearRight, m_startPlatformPosition.y});
	startSensor.push_back({startSensorNearRight, m_startPlatformPosition.y+m_sensorHeight});
	startSensor.push_back({startSensorNearLeft, m_startPlatformPosition.y+m_sensorHeight});
	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
	m_polygonShapes.back()->Set(startSensor.data(), startSensor.size());
	m_sensorShapes[START_GOOD] = m_polygonShapes.back().get();

	std::vector<b2Vec2> targetSensor;
	targetSensor.push_back({targetSensorNearLeft, m_targetPlatformPosition.y});
	targetSensor.push_back({targetSensorNearRight, m_targetPlatformPosition.y});
	targetSensor.push_back({targetSensorNearRight, m_targetPlatformPosition.y+m_sensorHeight});
	targetSensor.push_back({targetSensorNearLeft, m_targetPlatformPosition.y+m_sensorHeight});
	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
	m_polygonShapes.back()->Set(targetSensor.data(), targetSensor.size());
	m_sensorShapes[TARGET_GOOD] = m_polygonShapes.back().get();

	std::vector<b2Vec2> startSensorBadLeft;
	startSensorBadLeft.push_back({startSensorFarLeft, m_startPlatformPosition.y});
	startSensorBadLeft.push_back({startSensorNearLeft, m_startPlatformPosition.y});
	startSensorBadLeft.push_back({startSensorNearLeft, m_startPlatformPosition.y+m_sensorHeight});
	startSensorBadLeft.push_back({startSensorFarLeft, m_startPlatformPosition.y+m_sensorHeight});
	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
	m_polygonShapes.back()->Set(startSensorBadLeft.data(), startSensorBadLeft.size());
	m_sensorShapes[START_LEFT] = m_polygonShapes.back().get();

	std::vector<b2Vec2> startSensorBadRight;
	startSensorBadRight.push_back({startSensorNearRight, m_startPlatformPosition.y});
	startSensorBadRight.push_back({startSensorFarRight, m_startPlatformPosition.y});
	startSensorBadRight.push_back({startSensorFarRight, m_startPlatformPosition.y+m_sensorHeight});
	startSensorBadRight.push_back({startSensorNearRight, m_startPlatformPosition.y+m_sensorHeight});
	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
	m_polygonShapes.back()->Set(startSensorBadRight.data(), startSensorBadRight.size());
	m_sensorShapes[START_RIGHT] = m_polygonShapes.back().get();

	std::vector<b2Vec2> targetSensorBadLeft;
	targetSensorBadLeft.push_back({targetSensorFarLeft, m_targetPlatformPosition.y});
	targetSensorBadLeft.push_back({targetSensorNearLeft, m_targetPlatformPosition.y});
	targetSensorBadLeft.push_back({targetSensorNearLeft, m_targetPlatformPosition.y+m_sensorHeight});
	targetSensorBadLeft.push_back({targetSensorFarLeft, m_targetPlatformPosition.y+m_sensorHeight});
	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
	m_polygonShapes.back()->Set(targetSensorBadLeft.data(), targetSensorBadLeft.size());
	m_sensorShapes[TARGET_LEFT] = m_polygonShapes.back().get();

	std::vector<b2Vec2> targetSensorBadRight;
	targetSensorBadRight.push_back({targetSensorNearRight, m_targetPlatformPosition.y});
	targetSensorBadRight.push_back({targetSensorFarRight, m_targetPlatformPosition.y});
	targetSensorBadRight.push_back({targetSensorFarRight, m_targetPlatformPosition.y+m_sensorHeight});
	targetSensorBadRight.push_back({targetSensorNearRight, m_targetPlatformPosition.y+m_sensorHeight});
	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
	m_polygonShapes.back()->Set(targetSensorBadRight.data(), targetSensorBadRight.size());
	m_sensorShapes[TARGET_RIGHT] = m_polygonShapes.back().get();

//	sensorFixtureDef.shape = &targetSensorPolygon;
//
//	m_targetSensor.reset(new Physical(world, &m_bodyDefinitions[0]));
//	m_targetSensor->m_body->CreateFixture(&sensorFixtureDef);
//

//	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
//	m_polygonShapes.back()->Set(targetSensor.data(), targetSensor.size());
//	b2PolygonShape startSensorPolygon;
//	startSensorPolygon.Set(startSensor.data(), startSensor.size());
//	sensorFixtureDef.shape = &startSensorPolygon;
//
//	m_startSensor.reset(new Physical(world, &m_bodyDefinitions[0]));
//	m_startSensor->m_body->CreateFixture(&sensorFixtureDef);
//


//	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
//	m_polygonShapes.back()->Set(startSensor.data(), startSensor.size());
//
//	std::vector<b2Vec2> targetSensor;
//	targetSensor.push_back({targetSensorLeft, m_targetPlatformPosition.y});
//	targetSensor.push_back({targetSensorRight, m_targetPlatformPosition.y});
//	targetSensor.push_back({targetSensorRight, m_targetPlatformPosition.y+m_sensorHeight});
//	targetSensor.push_back({targetSensorLeft, m_targetPlatformPosition.y+m_sensorHeight});
//	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
//	m_polygonShapes.back()->Set(targetSensor.data(), targetSensor.size());

//	m_polygonShapes.back()->SetAsBox(m_startSensorWidth, m_sensorHeight);
//
//	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
//	m_polygonShapes.back()->SetAsBox(m_targetSensorWidth, m_sensorHeight);


//	std::cout<<"First point: "<<m_chainShapes.back()->m_vertices[0].x<<", "<<m_chainShapes.back()->m_vertices[0].y<<std::endl;
//	std::cout<<"First point: "<<m_chainShapes.back()->m_vertices[m_chainShapes.back()->m_count-1].x<<", "
//			<<m_chainShapes.back()->m_vertices[m_chainShapes.back()->m_count-1].y<<std::endl;

	//Create the physical representation
	m_physical.reset(new Physical(world, &m_bodyDefinitions[0]));

	//Create the graphical representaion
	m_mesh.reset(new Mesh);

    // Attach the polygons
    for (unsigned int polygon=0; polygon<m_polygonShapes.size(); polygon++)
    {
    	m_fixtureDefinitions[1].shape = m_polygonShapes[polygon].get();
    	m_physical->m_body->CreateFixture(&m_fixtureDefinitions[1]);
//    	m_mesh->give(m_polygonShapes[polygon]);
    }

    // Attach the circles
    for (unsigned int circle=0; circle<m_circleShapes.size(); circle++)
    {
    	m_fixtureDefinitions[0].shape = m_circleShapes[circle].get();
    	m_physical->m_body->CreateFixture(&m_fixtureDefinitions[0]);
    	m_mesh->give(m_circleShapes[circle]);
    }

    // Attach the chains
    for (unsigned int chain=0; chain<m_chainShapes.size(); chain++)
    {
    	m_fixtureDefinitions[0].shape = m_chainShapes[chain].get();
    	m_physical->m_body->CreateFixture(&m_fixtureDefinitions[0]);
    	m_mesh->give(m_chainShapes[chain]);
    }
}

LunarSurface::~LunarSurface()
{
	// TODO Auto-generated destructor stub
}

