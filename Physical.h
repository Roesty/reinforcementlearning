/*
 * Physical.h
 *
 *  Created on: Mar 21, 2022
 *      Author: mooseman
 */

#ifndef PHYSICAL_H_
#define PHYSICAL_H_

#include <memory>
#include <vector>
#include <glm/common.hpp>
#include <glm/vec3.hpp>

#include <Box2D/Box2D.h>

class Physical
{
public:

	//The body
	b2Body* m_body;

	void setPosition(glm::vec3 position);
	glm::vec3 getPosition();

	Physical();
	Physical(b2World* world, b2BodyDef* bodyDef);
	virtual ~Physical();
};

#endif /* PHYSICAL_H_ */
