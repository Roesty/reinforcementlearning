/*
 * SceneLunarLander.cpp
 *
 *  Created on: Mar 17, 2022
 *      Author: mooseman
 */

#include "SceneLunarLander.h"

#include <cmath>
#include <iostream>

SceneLunarLander SceneLunarLander::m_sceneLunarLander;

void SceneLunarLander::init(SceneManager *sm)
{
	m_sm = sm;

	m_name = "Lunar Lander";

	m_up = b2Vec2(0.0f, 1.0f);
	m_down = b2Vec2(0.0f, -1.0f);
	m_left = b2Vec2(-1.0f, 0.0f);
	m_right = b2Vec2(1.0f, 0.0f);

	m_formatW = "%4.2f kg";
	m_formatS = "%4.2f m/sec";
	m_formatD = "%4.2f m";
	m_formatT = "%4.2f sec";
	m_formatScore = "%4.2f ";
	m_formatInt = "%i";

	m_episodeDurationMax = 15.0f;
	m_episodeDuration = 0.0f;

	m_boundary = b2Vec2(200.0f, 200.0f);

	m_simulationGenerationSize = 100;
	m_generationsToEvolve = 1;

	m_leftContact = true;
	m_rightContact = true;

	m_isWireframe = false;
	m_isFullscreen = false;

	m_clearColor.x = 0.0f;
	m_clearColor.y = 0.0f;
	m_clearColor.z = 0.0f;

	ImGui::CreateContext();
	ImGui_ImplSDL2_InitForOpenGL(sm->m_window, &sm->m_context);
	ImGui_ImplOpenGL3_Init();

	m_event = new SDL_Event();

	m_shader.reset(new Shader("default.vert", "default.frag"));

	//Box2D setup
	m_gravity.reset(new b2Vec2(0.0f, -1.625f));
	m_world.reset(new b2World(*m_gravity));
	m_velocityIterations = 6;
	m_positionIterations = 2;
	m_timeStep = 0.1f;

	m_episodeStepsMax = 15 * 60;
	m_episodeStepsMax = 1000;
	m_episodeSteps = 0;

	m_simulatedEpisodeStepsMax = 1000;
	m_simulatedEpisodeSteps = 0;

	//Create the entities
	m_lunarSurface.reset(new LunarSurface(m_world.get()));
	m_lander.reset(new Lander(m_world.get()));

	m_landerStartingPosition.x = m_lunarSurface->m_startPlatformPosition.x;
	m_landerStartingPosition.y = m_lunarSurface->m_startPlatformPosition.y + m_lander->m_height;
	m_landerStartingPosition.z = 0.0f;

	m_landerTargetPosition.x = m_lunarSurface->m_targetPlatformPosition.x;
	m_landerTargetPosition.y = m_lunarSurface->m_targetPlatformPosition.y + m_lander->m_height;
	m_landerTargetPosition.z = 0.0f;

	m_timeProgressbarColor.r = 0.4f;
	m_timeProgressbarColor.g = 0.2f;
	m_timeProgressbarColor.b = 0.5f;

	m_flagTargetLeft.reset(new Flag(m_world.get()));
    m_flagTargetLeft->m_color = m_leftFlagColor;
    m_flagTargetLeft->m_faceRight = false;
    m_flagTargetLeft->m_base = b2Vec2(m_lunarSurface->m_targetPlatformPosition.x-m_lunarSurface->m_targetSensorWidth/2.0f, m_lunarSurface->m_targetPlatformPosition.y);
    m_flagTargetLeft->construct(m_world.get());

    m_flagTargetRight.reset(new Flag(m_world.get()));
    m_flagTargetRight->m_color = m_rightFlagColor;
    m_flagTargetRight->m_faceRight = true;
    m_flagTargetRight->m_base = b2Vec2(m_lunarSurface->m_targetPlatformPosition.x+m_lunarSurface->m_targetSensorWidth/2.0f, m_lunarSurface->m_targetPlatformPosition.y);
    m_flagTargetRight->construct(m_world.get());

	resetLanderPosition();

	//Setup the camera
	m_camera.setMPosition(glm::vec3(0.0f, 0.0f, 30.0f));
	m_camera.setMOrientation(glm::vec3(0.0f, 0.0f, -1.0f));
	m_camera.setMUp(glm::vec3(0.0f, 1.0f, 0.0f));

	m_camera.setMSpeed(50.0f);
	m_camera.setMSensitivity(8.0f);
	m_camera.setMZoomSensitivity(10.0f);

	m_camera.setMYaw(-90.0f);
	m_camera.setMPitch(0.0f);

	m_camera.setMMinFov(10.0f);
	m_camera.setMMaxFov(120.0f);
	m_camera.setMCurFov(90.0f);

	m_camera.setMOrthoLeft(-m_lunarSurface->m_width / 2.0f);
	m_camera.setMOrthoRight(m_lunarSurface->m_width / 2.0f);

	m_camera.setMOrthoTop(50.0f);
	m_camera.setMOrthoBottom(-2.0f);

	m_camera.setMOrthoNear(0.0f);
	m_camera.setMOrthoFar(100.0f);

	m_camera.setMProjectionMode(OPRTHOGRAPHIC);
}

void SceneLunarLander::cleanup()
{
}

void SceneLunarLander::pause()
{
}

void SceneLunarLander::resume()
{
}

void SceneLunarLander::handleEvents(SceneManager *sm)
{
	while (SDL_PollEvent(m_event))
	{
		ImGui_ImplSDL2_ProcessEvent(m_event);

		if (m_event->type == SDL_KEYUP)
		{
			switch (m_event->key.keysym.sym)
			{
			case SDLK_ESCAPE:
				sm->quit();
				break;
			case 'w':
			{
				m_thrustUp = false;
				break;
			}
			case 's':
			{
				m_thrustDown = false;
				break;
			}
			case 'a':
			{
				m_thrustLeft = false;
				break;
			}
			case 'd':
			{
				m_thrustRight = false;
				break;
			}
			}

		}

		if (m_event->type == SDL_KEYDOWN)
		{
			switch (m_event->key.keysym.sym)
			{
			case SDLK_ESCAPE:
				sm->quit();
				break;
			case 'w':
			{
				m_thrustUp = true;
				float force = m_lander->m_thrusterPower;
				float mass = m_lander->getMass();
				b2Vec2 direction = m_up;
				m_lander->m_physical->m_body->ApplyForceToCenter(force * mass * direction, true);

				break;
			}
			case 's':
			{
				m_thrustDown = true;
				break;
			}
			case 'a':
			{
				m_thrustLeft = true;
				break;
			}
			case 'd':
			{
				m_thrustRight = true;
				break;
			}
			case 'p':
			{
				if (m_camera.getMProjectionMode() == eProjections::PERSPECTIVE)
				{
					m_camera.setMProjectionMode(OPRTHOGRAPHIC);
				}
				else
				{
					m_camera.setMProjectionMode(PERSPECTIVE);
				}
				break;
			}
			case 'l':
			{
				m_isWireframe = !m_isWireframe;
				if (m_isWireframe)
				{
					glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
				}
				else
				{
					glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				}
				break;
			}
			case 'f':
				m_isFullscreen = !m_isFullscreen;
				if (m_isFullscreen)
				{
					SDL_SetWindowFullscreen(sm->m_window, sm->m_winFlags | SDL_WINDOW_FULLSCREEN_DESKTOP);

					int width, height;
					SDL_GetWindowSize(sm->m_window, &width, &height);
					std::cout << "Fullscreen window size: " << width << "x" << height << std::endl;
				}
				else
				{
					SDL_SetWindowFullscreen(sm->m_window, sm->m_winFlags);
				}
				break;
			default:
				//					std::cout<<"Key not implemented"<<std::endl;
				break;
			}
		}

		if (m_event->type == SDL_QUIT)
		{
			sm->quit();
		}

		//		camera.handleMouseInput(&event);
	}
}

void SceneLunarLander::update(SceneManager *sm)
{
	b2Vec2 landerPosition(abs(m_lander->getPosition().x), abs(m_lander->getPosition().y));

	m_leftContact = isLeftFootLanded();
	m_rightContact = isRightFootLanded();
	m_hullContact = isHullColliding();

	float force = m_lander->m_thrusterPower;
	float mass = m_lander->getMass();
	m_lander->m_physical->m_body->ApplyForceToCenter(force * mass * getThrustDirection(), true);

	updateLanderInput();
//	m_lander->setInValue(0, getToTargetDistance().x);
//	m_lander->setInValue(1, getToTargetDistance().y);
//
//	m_lander->setInValue(2, m_lander->getVelocity().x);
//	m_lander->setInValue(3, m_lander->getVelocity().y);
//
//	m_lander->setInValue(4, m_leftContact);
//	m_lander->setInValue(5, m_rightContact);

	m_lander->act(this);

	m_world->Step(m_timeStep, m_velocityIterations, m_positionIterations);
	m_episodeSteps++;

	m_activeCollisions = m_world->GetContactCount();

	if (landerLeftBoundary() || stepLimitReached() || isHullColliding())
	{
		resetEpisode();
	}

	updateTime();
}

void SceneLunarLander::draw(SceneManager *sm)
{
	//Clear the screen
	glClearColor(m_clearColor.x, m_clearColor.y, m_clearColor.z, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	startGui(sm);

	int viewLoc = glGetUniformLocation(m_shader->ID, "view");
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(m_camera.getView()));

	int projectionLoc = glGetUniformLocation(m_shader->ID, "projection");
	glUniformMatrix4fv(projectionLoc, 1, GL_FALSE, glm::value_ptr(m_camera.getProjection()));

	glm::mat4 trans = glm::mat4(1.0f);
	unsigned int transformLoc = glGetUniformLocation(m_shader->ID, "transform");
	glUniformMatrix4fv(transformLoc, 1, GL_FALSE, glm::value_ptr(trans));

	//Activate shader
	m_shader->use();

	//Activate textures
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, sm->m_spriteSheetTextureID);

	m_lunarSurface->draw(*m_shader);
	m_flagTargetLeft->draw(*m_shader);
	m_flagTargetRight->draw(*m_shader);

	glClear(GL_DEPTH_BUFFER_BIT);

	m_lander->draw(*m_shader);

	//Draw the GUI
	drawGui(sm);

	//Output FPS in window title
	updateTitle(sm);

	//	SDL_GL_SetSwapInterval(1);

	endGui(sm);

	SDL_GL_SwapWindow(sm->m_window);
}

void SceneLunarLander::startGui(SceneManager *sm)
{
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplSDL2_NewFrame(sm->m_window);
	ImGui::NewFrame();
}

void SceneLunarLander::addWindowSize()
{
	m_nextWindowPosition.y += ImGui::GetWindowHeight();
}

void SceneLunarLander::guiLanderInfo()
{
	m_nextWindowSize.y = 400;

	ImGui::SetNextWindowSize({m_nextWindowSize.x, m_nextWindowSize.y});
	ImGui::SetNextWindowPos({m_nextWindowPosition.x, m_nextWindowPosition.y});

	ImGui::Begin("Lander controls");

	ImGui::LabelText("Lander mass", m_formatW, m_lander->getMass());
	ImGui::LabelText("Position X", m_formatD, m_lander->getPosition().x);
	ImGui::LabelText("Position Y", m_formatD, m_lander->getPosition().y);
	ImGui::LabelText("Velocity X", m_formatS, m_lander->getVelocity().x);
	ImGui::LabelText("Velocity Y", m_formatS, m_lander->getVelocity().y);
	ImGui::LabelText("To X", m_formatD, getToTargetDistance().x);
	ImGui::LabelText("To Y", m_formatD, getToTargetDistance().y);

	ImGui::Checkbox("Left contact", &m_leftContact);
	ImGui::Checkbox("Right contact", &m_rightContact);
	ImGui::Checkbox("Hull contact", &m_hullContact);
	ImGui::SliderFloat("Power", &m_lander->m_thrusterPower, 0.0f, 10.0f);
//	ImGui::SliderFloat("Lander rotation", &m_testFloat, -100.0f, 100.0f);

	if (ImGui::Button("Reset Lander"))
	{
		resetEpisode();
	}

	if (ImGui::Button("Randomise"))
	{
		m_lander->m_neuralNetwork->randomise();
		resetEpisode();
	}

	addWindowSize();
	ImGui::End();
}

void SceneLunarLander::guiNeuralNetworkInfo()
{
	m_nextWindowSize.y = 250;

	ImGui::SetNextWindowSize({m_nextWindowSize.x, m_nextWindowSize.y});
	ImGui::SetNextWindowPos({m_nextWindowPosition.x, m_nextWindowPosition.y});

	ImGui::Begin("Neural Network information");

	ImGui::Text("In");
	for (int iii = 0; iii < m_lander->m_inNNSize; iii++)
	{
		std::string label = std::to_string(iii);
		ImGui::Value(label.c_str(), m_lander->getInValue(iii));
	}

	ImGui::Spacing();
	ImGui::Text("Out");
	for (int iii = 0; iii < m_lander->m_outNNSize; iii++)
	{
		std::string label = std::to_string(iii);
		ImGui::Value(label.c_str(), m_lander->getOutValue(iii));

	}
	addWindowSize();
	ImGui::End();
}


void SceneLunarLander::guiSimulationInfo()
{
	m_nextWindowSize.y = 100;

	ImGui::SetNextWindowSize({m_nextWindowSize.x, m_nextWindowSize.y});
	ImGui::SetNextWindowPos({m_nextWindowPosition.x, m_nextWindowPosition.y});

	ImGui::Begin("Simulation information");

//	ImGui::LabelText("Max time", formatT, m_episodeDurationMax);
	ImGui::LabelText("Elapsed", m_formatT, m_episodeDuration);

	float stepFraction = (float) m_episodeSteps / (float) m_episodeStepsMax;

	ImGui::PushID(1);

	ImGui::PushStyleColor(ImGuiCol_PlotHistogram,
	{m_timeProgressbarColor.r, m_timeProgressbarColor.g, m_timeProgressbarColor.b, 1.0f});

	std::string progress = std::to_string(int(stepFraction * 100.0f)) + "%";

//	ImGui::ProgressBar(timeFraction, ImVec2(width*0.8f, 20.0f), "Time");
//	ImGui::ProgressBar(timeFraction, ImVec2(0.0f, 0.0f), progress.c_str());

	progress = "";
	progress += std::to_string(m_episodeSteps);
	progress += " / ";
	progress += std::to_string(m_episodeStepsMax);

	ImGui::ProgressBar(stepFraction, ImVec2(0.0f, 0.0f), progress.c_str());
	ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
	ImGui::Text("Steps");

	ImGui::PopStyleColor(1);
	ImGui::PopID();

	ImGui::LabelText("Ended", m_formatInt, m_episodeStepsLast);

	addWindowSize();
	ImGui::End();
}

void SceneLunarLander::guiTestControls()
{
	m_nextWindowSize.y = 150;

	ImGui::SetNextWindowSize({m_nextWindowSize.x, m_nextWindowSize.y});
	ImGui::SetNextWindowPos({m_nextWindowPosition.x, m_nextWindowPosition.y});

	ImGui::Begin("Test controls");

	if (ImGui::Button("Run test", ImVec2(0.0f, 0.0f)))
	{
		resetEpisode();

		m_testStartTime = m_sm->getTime();
		m_testScore = runEpisode(m_sm);
		m_testEndTime = m_sm->getTime();
	}

	std::string result = "Test result: ";
	result += m_simulatedEpisodeResult;

	ImGui::Text(result.c_str());
	ImGui::LabelText("Score", m_formatScore, m_testScore);
	ImGui::LabelText("Time", m_formatT, getTestDuration());

	addWindowSize();
	ImGui::End();
}

void SceneLunarLander::guiNeuralNetworkVisualisation()
{
	m_nextWindowPosition.x = m_nextWindowSize.x;
	m_nextWindowPosition.y = 0.0f;

	m_nextWindowSize.x = 700;
	m_nextWindowSize.y = 500;

	ImGui::SetNextWindowSize({m_nextWindowSize.x, m_nextWindowSize.y});
	ImGui::SetNextWindowPos({m_nextWindowPosition.x, m_nextWindowPosition.y});

	ImGui::Begin("Neural Network Visualisation");

	ImGui::PushItemWidth(-ImGui::GetFontSize() * 15);
	ImDrawList* drawList = ImGui::GetWindowDrawList();

	std::vector<std::vector<ImVec2>> centers;

	ImVec2 vMin = ImGui::GetWindowContentRegionMin();
	ImVec2 vMax = ImGui::GetWindowContentRegionMax();

	vMin.x += ImGui::GetWindowPos().x;
	vMin.y += ImGui::GetWindowPos().y;
	vMax.x += ImGui::GetWindowPos().x;
	vMax.y += ImGui::GetWindowPos().y;

	float drawAreaWidth = vMax.x - vMin.x;
	float drawAreaHeight = vMax.y - vMin.y;

	float radius = 30.0f;

	float paddingHor = radius;
	float paddingVer = 0.0f;

	float diameter = radius*2.0f;

	float borderWidth = 3.0f;
	float halfBorderWidth = borderWidth / 2.0f;

	std::vector<int> layerSizes = m_lander->m_neuralNetwork->m_sizes;

	float spaceHor = drawAreaWidth;
	spaceHor -= paddingHor*2.0f;
	spaceHor /= layerSizes.size()-1;

	for (unsigned int layer=0; layer < layerSizes.size(); layer++)
	{
		centers.push_back(std::vector<ImVec2>());

		float spaceVer = drawAreaHeight;

		spaceVer -= paddingVer*2.0f;
		spaceVer /= layerSizes[layer];

		for (int node=0; node<layerSizes[layer]; node++)
		{
			ImVec2 center = vMin;

			center.x += paddingHor;
//			center.x += spaceHor/2.0f;
			center.x += spaceHor*layer;

			center.y += paddingVer;
			center.y += spaceVer/2.0f;
			center.y += spaceVer*node;

			centers.back().push_back(center);

//			ImVec4 borderColorDef(0.9f, 0.9f, 0.9f, 1.0f);
//			ImVec4 fillColorDef(0.2f, 0.5f, 0.2f, 1.0f);
//
//			ImU32 borderColor = ImColor(borderColorDef);
//			ImU32 fillColor = ImColor(fillColorDef);
//
//			drawList->AddCircle(center, radius, borderColor, 0, borderWidth);
//			drawList->AddCircleFilled(center, radius-halfBorderWidth, fillColor);
		}
	}

	ImVec4 borderColorDef(0.0f, 0.0f, 0.0f, 1.0f);
	ImVec4 fillColorDef(0.2f, 0.5f, 0.2f, 1.0f);
	ImVec4 lineColorDef(0.7f, 0.2f, 0.2f, 0.7f);

//	ImVec4 borderColorDef(0.9f, 0.9f, 0.9f, 1.0f);

	ImVec4 negativeFillColorDef(0.8f, 0.0f, 0.0f, 1.0f);
	ImVec4 positiveFillColorDef(0.0f, 0.8f, 0.0f, 1.0f);

	ImVec4 negativeLineColorDef(0.7f, 0.2f, 0.0f, 0.3f);
	ImVec4 positiveLineColorDef(0.2f, 0.7f, 0.0f, 0.3f);

	ImU32 borderColor = ImColor(borderColorDef);
	ImU32 lineColor = ImColor(lineColorDef);

	b2Vec2 inRange(-1.0f, 1.0f);
	b2Vec2 outRange(0.0f, 1.0f);

	float lineWidth = 2.0f;

	for (unsigned int space = 1; space<centers.size(); space++)
	{
		int leftSize = layerSizes[space-1];
		int rightSize = layerSizes[space];

		for (int leftNode=0; leftNode<leftSize; leftNode++)
		{
			ImVec2 v0 = centers[space-1][leftNode];

			for (int rightNode=0; rightNode<rightSize; rightNode++)
			{
				ImVec2 v1 = centers[space][rightNode];

				float t = m_lander->m_neuralNetwork->m_layers[space-1](leftNode, rightNode);
				t = map(t, inRange, outRange);

				lineColor = getColorLerpImGui(negativeFillColorDef, positiveFillColorDef, t);

				drawList->AddLine(v0, v1, lineColor, lineWidth);

			}
		}
	}

	for (unsigned int layer = 0; layer<centers.size(); layer++)
	{
		for (unsigned int node=0; node<centers[layer].size(); node++)
		{

			ImVec2 center = centers[layer][node];

			float t = m_lander->m_neuralNetwork->getNodeValue(layer, node);

//			std::cout<<t<<" -> ";
			t = map(t, inRange, outRange);
//			std::cout<<t<<std::endl;

			ImU32 fillColor = getColorLerpImGui(negativeFillColorDef, positiveFillColorDef, t);

			drawList->AddCircle(center, radius, borderColor, 0, borderWidth);
			drawList->AddCircleFilled(center, radius-halfBorderWidth, fillColor);
		}
	}

	ImGui::PopItemWidth();

	ImGui::End();
}

void SceneLunarLander::guiEvolutionSimulatorControls()
{
	m_nextWindowSize.y = 150;

	ImGui::SetNextWindowSize({m_nextWindowSize.x, m_nextWindowSize.y});
	ImGui::SetNextWindowPos({m_nextWindowPosition.x, m_nextWindowPosition.y});

	ImGui::Begin("Evolution controls");

	ImGui::SliderInt("Generations", &m_generationsToEvolve, 1, 100);

	if (ImGui::Button("Evolve", ImVec2(0.0f, 0.0f)))
	{
		m_evolutionStartTime = m_sm->getTime();

		evolve(m_generationsToEvolve);

		m_evolutionEndTime = m_sm->getTime();
	}

	float timeElapsed = m_evolutionEndTime-m_evolutionStartTime;

	if (m_evolutionBestScores.size())
	{
		ImGui::LabelText("Time", m_formatT, timeElapsed);
		ImGui::LabelText("Best Score", m_formatScore, m_evolutionBestScores.back());

		if (ImGui::Button("Show best"))
		{
//			resetLander();
			resetEpisode();
			m_lander->m_neuralNetwork->set(m_bestNeuralNetwork);
		}
	}

	addWindowSize();
	ImGui::End();
}

void SceneLunarLander::drawGui(SceneManager *sm)
{
	float winX, winY;
	float width, height;

	m_nextWindowPosition.SetZero();
	m_nextWindowSize.x = 300;

//	guiLanderInfo();
	guiEvolutionSimulatorControls();
//	guiNeuralNetworkInfo();
	guiSimulationInfo();
	guiTestControls();
	guiNeuralNetworkVisualisation();

	width = 400;
	height = 80;
	ImGui::SetNextWindowSize(
	{width, height});

	winX = sm->m_winWidth - 300.0f;
	winY = 0.0f;
	ImGui::SetNextWindowPos(
	{winX, winY});

	ImGui::SetNextWindowBgAlpha(0.0f);

	ImGui::Begin("FPS");
	ImGui::Text("FPS: %i", sm->getFPS());
	ImGui::Text("Time since last frame: %f seconds", sm->m_deltaTime);
	ImGui::Text("Active collisions: %i", m_activeCollisions);
	ImGui::End();
}

void SceneLunarLander::endGui(SceneManager *sm)
{
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

SceneLunarLander::SceneLunarLander()
{
	// TODO Auto-generated constructor stub
}

b2Vec2 SceneLunarLander::getThrustDirection()
{
	b2Vec2 direction(0.0f, 0.0f);

	if (m_thrustUp)
	{
		direction.y += 1;
	}

	if (m_thrustDown)
	{
		direction.y -= 1;
	}

	if (m_thrustRight)
	{
		direction.x += 1;
	}

	if (m_thrustLeft)
	{
		direction.x -= 1;
	}

	return direction;
}

void SceneLunarLander::resetLanderPosition()
{
	m_lander->setPosition(m_landerStartingPosition);
}

void SceneLunarLander::resetLanderVelocity()
{
	m_lander->setVelocity(0.0f, 0.0f);
}

void SceneLunarLander::resetLanderAngle()
{
	m_lander->setAngle(0.0f);
	m_lander->m_physical->m_body->SetAngularVelocity(0.0f);
}

void SceneLunarLander::resetLander()
{
	resetLanderPosition();
	resetLanderVelocity();
	resetLanderAngle();
}

void SceneLunarLander::resetEpisode()
{
	resetLander();
	m_episodeStepsLast = m_episodeSteps;
	m_episodeStart = m_sm->getTime();
	m_episodeSteps = 0;
}

glm::vec3 SceneLunarLander::getToTargetDistance()
{
	glm::vec3 to = m_landerTargetPosition;

	glm::vec3 from(0.0f);

	if (m_lander.get())
	{
		from = m_lander->getPosition();
	}
//	m_lander->getMPosition();
//		printVec3(m_lander->getMPosition(), "FROM");
//		std::cout<<m_lander->getMPosition().x<<std::endl;

	return to - from;
}

bool SceneLunarLander::landerLeftBoundary()
{
	b2Vec2 landerPosition(abs(m_lander->getPosition().x), abs(m_lander->getPosition().y));

	if (landerPosition.x > m_boundary.x || landerPosition.y > m_boundary.y)
	{
		return true;
	}

	return false;
}

void SceneLunarLander::updateTime()
{
	m_episodeDuration = m_sm->getTime() - m_episodeStart;
}

bool SceneLunarLander::timeExpired()
{
	return m_episodeDuration >= m_episodeDurationMax;
}

bool SceneLunarLander::isLeftFootLanded()
{
	return getFootLandingStatus().y;
}

bool SceneLunarLander::isRightFootLanded()
{
	return getFootLandingStatus().x;
}

b2Vec2 SceneLunarLander::getFootLandingStatus()
{
	//https://stackoverflow.com/a/9396772

	b2Transform landerTransform = m_lander->m_physical->m_body->GetTransform();
	b2Transform lunarSurfaceTransform = m_lunarSurface->m_physical->m_body->GetTransform();

	b2Shape* leftFootShape = m_lander->m_footShapes[Lander::LEFT];
	b2Shape* rightFootShape = m_lander->m_footShapes[Lander::RIGHT];

	b2Shape* startSensorShape = m_lunarSurface->m_sensorShapes[LunarSurface::START_GOOD];
	b2Shape* targetSensorShape = m_lunarSurface->m_sensorShapes[LunarSurface::TARGET_GOOD];

	b2Shape* startSensorLeft = m_lunarSurface->m_sensorShapes[LunarSurface::START_LEFT];
	b2Shape* startSensorRight = m_lunarSurface->m_sensorShapes[LunarSurface::START_RIGHT];

	b2Shape* targetSensorLeft = m_lunarSurface->m_sensorShapes[LunarSurface::TARGET_LEFT];
	b2Shape* targetSensorRight = m_lunarSurface->m_sensorShapes[LunarSurface::TARGET_RIGHT];

	bool leftLanded = false;
	bool rightLanded = false;

	bool startPlatformValid = false;

	if (startPlatformValid)
	{
		if (b2TestOverlap(leftFootShape, 0, startSensorShape, 0, landerTransform, lunarSurfaceTransform)
				|| b2TestOverlap(leftFootShape, 0, targetSensorShape, 0, landerTransform, lunarSurfaceTransform))
		{
			leftLanded = true;
		}

		if (b2TestOverlap(rightFootShape, 0, startSensorShape, 0, landerTransform, lunarSurfaceTransform)
				|| b2TestOverlap(rightFootShape, 0, targetSensorShape, 0, landerTransform, lunarSurfaceTransform))
		{
			rightLanded = true;
		}
	}
	else
	{
		if (b2TestOverlap(leftFootShape, 0, targetSensorShape, 0, landerTransform, lunarSurfaceTransform))
		{
			leftLanded = true;
		}

		if (b2TestOverlap(rightFootShape, 0, targetSensorShape, 0, landerTransform, lunarSurfaceTransform))
		{
			rightLanded = true;
		}
	}

	if (b2TestOverlap(leftFootShape, 0, startSensorLeft, 0, landerTransform, lunarSurfaceTransform)
			|| b2TestOverlap(leftFootShape, 0, startSensorRight, 0, landerTransform, lunarSurfaceTransform)
			|| b2TestOverlap(leftFootShape, 0, targetSensorLeft, 0, landerTransform, lunarSurfaceTransform)
			|| b2TestOverlap(leftFootShape, 0, targetSensorRight, 0, landerTransform, lunarSurfaceTransform))
	{
		leftLanded = false;
	}

	if (b2TestOverlap(rightFootShape, 0, startSensorLeft, 0, landerTransform, lunarSurfaceTransform)
			|| b2TestOverlap(rightFootShape, 0, startSensorRight, 0, landerTransform, lunarSurfaceTransform)
			|| b2TestOverlap(rightFootShape, 0, targetSensorLeft, 0, landerTransform, lunarSurfaceTransform)
			|| b2TestOverlap(rightFootShape, 0, targetSensorRight, 0, landerTransform, lunarSurfaceTransform))
	{
		rightLanded = false;
	}

	return b2Vec2(leftLanded, rightLanded);
}

bool SceneLunarLander::isHullColliding()
{
	b2Fixture* lunarSurfaceFixture = m_lunarSurface->m_physical->m_body->GetFixtureList();

	while (lunarSurfaceFixture->GetNext())
	{
		bool isSensor = lunarSurfaceFixture->IsSensor();

		if (!isSensor)
		{
			b2Shape* surfaceShape = lunarSurfaceFixture->GetShape();
			b2Shape* hullShape = m_lander->m_hullSensorShape;

			b2Transform landerTransform = m_lander->m_physical->m_body->GetTransform();
			b2Transform lunarSurfaceTransform = m_lunarSurface->m_physical->m_body->GetTransform();

			int edges = surfaceShape->GetChildCount();

			for (int edge=0; edge<edges; edge++)
			{
				if (b2TestOverlap(hullShape, 0, surfaceShape, edge, landerTransform, lunarSurfaceTransform))
				{
					return true;
				}
			}
		}

		lunarSurfaceFixture = lunarSurfaceFixture->GetNext();
	}

	return false;
}

float SceneLunarLander::runEpisode(SceneManager* sm)
{
	float score = 0.0f;
	m_testScore = 0.0f;

	m_lander->m_neuralNetwork->randomise();
	resetLander();

	m_simulatedEpisodeSteps = 0;

	bool episodeRunning=true;
	while (episodeRunning)
	{
		b2Vec2 landerPosition(abs(m_lander->getPosition().x), abs(m_lander->getPosition().y));

		m_leftContact = isLeftFootLanded();
		m_rightContact = isRightFootLanded();
		m_hullContact = isHullColliding();


		updateLanderInput();
		m_lander->act(this);

		m_world->Step(m_timeStep, m_velocityIterations, m_positionIterations);
		m_simulatedEpisodeSteps++;

//		score = getDistanceScore();
		increaseScore();

		if (landerLeftBoundary())
		{
			m_simulatedEpisodeResult = "Exited boundary at "+std::to_string(m_simulatedEpisodeSteps);
			episodeRunning = false;
		}

		if (simulationStepLimitReached())
		{
			m_simulatedEpisodeResult = "Steps exceeded at "+std::to_string(m_simulatedEpisodeSteps);
			episodeRunning = false;
		}

		if (isHullColliding())
		{
			m_simulatedEpisodeResult = "Hull collided at "+std::to_string(m_simulatedEpisodeSteps);
			episodeRunning = false;
		}
	}
	resetLander();

	return m_testScore;
}

void SceneLunarLander::updateLanderInput()
{
	m_lander->setInValue(0, getToTargetDistance().x);
	m_lander->setInValue(1, getToTargetDistance().y);

	m_lander->setInValue(2, m_lander->getVelocity().x);
	m_lander->setInValue(3, m_lander->getVelocity().y);

	m_lander->setInValue(4, m_leftContact, false);
	m_lander->setInValue(5, m_rightContact, false);
}

bool SceneLunarLander::stepLimitReached()
{
	return m_episodeSteps >= m_episodeStepsMax;
}

float SceneLunarLander::getTestDuration()
{
	return m_testEndTime - m_testStartTime;
}

float SceneLunarLander::getDistanceScore()
{
	return getBaseDistance() - glm::length(getToTargetDistance());
}

void SceneLunarLander::increaseScore()
{
	float increase = 1.0f/(float)m_simulatedEpisodeSteps;


	float remainingTime = m_simulatedEpisodeStepsMax-m_simulatedEpisodeSteps;
	remainingTime /= (float)m_simulatedEpisodeStepsMax;

	increase = remainingTime*getDistanceScore();

	float multiplier = 1.0f + getFootLandingStatus().x + getFootLandingStatus().y;

//	increase *= (float)m_simulatedEpisodeSteps/(float)m_simulatedEpisodeStepsMax;
//	increase *= getDistanceScore();

	m_testScore += increase*multiplier;
}

float SceneLunarLander::getBaseDistance()
{
	glm::vec3 to = m_landerTargetPosition;
	glm::vec3 from = m_landerStartingPosition;

	return glm::length(to - from);
}

ImU32 SceneLunarLander::getColor(int r, int g, int b, int a)
{
	ImU32 color=0;
	color += r;
	color <<= 8;
	color += g;
	color <<= 8;
	color += b;
	color <<= 8;
	color += a;
	color <<= 8;
	return color;
}

ImU32 SceneLunarLander::getColor(glm::vec3 color)
{
	return getColor(color.x, color.y, color.z);
}

float SceneLunarLander::map(float v, float i0, float i1, float o0, float o1)
{
	return (v - i0) / (i1 - i0) * (o1 - o0) + o0;
	return (v - i0) * (o1 - o1) / (i1 - i0) + o0;
}

float SceneLunarLander::map(float v, b2Vec2 i, b2Vec2 o)
{
	return map(v, i.x, i.y, o.x, o.y);
}

ImU32 SceneLunarLander::getColorLerpImGui(ImVec4 low, ImVec4 high, float t)
{
	float r = lerp(low.x, high.x, t);
	float g = lerp(low.y, high.y, t);
	float b = lerp(low.z, high.z, t);
	float a = lerp(low.w, high.w, t);

	return ImColor(r, g, b, a);
}

float SceneLunarLander::lerp(float low, float high, float t)
{
	return low + ((high - low) * t);
}

void SceneLunarLander::evolve(int generations)
{
	if (generations > 1)
	{
		evolve(generations-1);
//		return;
	}

	if (m_neuralNetworks.size() != m_simulationGenerationSize)
	{
		m_neuralNetworks.clear();

		for (int specimen=0; specimen<m_simulationGenerationSize; specimen++)
		{
			std::vector<int> middleLayers;
			middleLayers.push_back(6);
			middleLayers.push_back(5);
			middleLayers.push_back(4);
			m_neuralNetworks.push_back(NeuralNetwork(m_lander->m_inNNSize, m_lander->m_outNNSize, middleLayers));
		}
	}

	m_generationScores.clear();

	for (unsigned int specimen=0; specimen<m_neuralNetworks.size(); specimen++)
	{
		m_evolutionTestScore = 0.0f;

		m_lander->m_neuralNetwork->set(m_neuralNetworks[specimen]);

		resetLander();
		m_simulatedEpisodeSteps = 0;

//		score = getDistanceScore();
		increaseScore();

		bool episodeRunning=true;
		while (episodeRunning)
		{
			b2Vec2 landerPosition(abs(m_lander->getPosition().x), abs(m_lander->getPosition().y));

			m_leftContact = isLeftFootLanded();
			m_rightContact = isRightFootLanded();
			m_hullContact = isHullColliding();

			m_lander->act(this);

			m_world->Step(m_timeStep, m_velocityIterations, m_positionIterations);
			m_simulatedEpisodeSteps++;

			increaseScore();

			if (landerLeftBoundary())
			{
//				m_simulatedEpisodeResult = "Exited boundary at "+std::to_string(m_simulatedEpisodeSteps);
				episodeRunning = false;
			}

			if (simulationStepLimitReached())
			{
//				m_simulatedEpisodeResult = "Steps exceeded at "+std::to_string(m_simulatedEpisodeSteps);
				episodeRunning = false;
			}

			if (isHullColliding())
			{
//				m_simulatedEpisodeResult = "Hull collided at "+std::to_string(m_simulatedEpisodeSteps);
				episodeRunning = false;
			}
		}

		m_evolutionTestScore = m_testScore;

		m_generationScores.push_back(m_evolutionTestScore);

		resetLander();
	}

    // Vector to store element
    // with respective present index
    std::vector<std::pair<float, int> > vp;

    for (unsigned int iii = 0; iii < m_generationScores.size(); ++iii)
    {
        vp.push_back(std::make_pair(m_generationScores[iii], iii));
    }

    sort(vp.begin(), vp.end());
//    std::reverse(vp.begin(),vp.end());

    m_evolutionBestScores.push_back(vp.back().first);

    int halfGenSize = m_simulationGenerationSize/2;

    //Set the back half to be variations of the better half
    for (int iii=0; iii<halfGenSize; iii++)
    {
    	int badIndex = vp[iii].second;
    	int goodIndex = vp[iii+halfGenSize].second;

    	m_neuralNetworks[badIndex].setVariant(m_neuralNetworks[goodIndex]);
    }

    m_bestNeuralNetwork.set(m_neuralNetworks[vp.back().second]);
}

bool SceneLunarLander::simulationStepLimitReached()
{
	return m_simulatedEpisodeSteps>= m_simulatedEpisodeStepsMax;
}





















