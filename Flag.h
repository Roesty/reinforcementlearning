/*
 * Flag.h
 *
 *  Created on: Mar 27, 2022
 *      Author: mooseman
 */

#ifndef FLAG_H_
#define FLAG_H_

#include "Entity.h"

#include <glm/common.hpp>

class Flag: public Entity
{
public:

	float m_poleWidth;
	float m_poleHeight;

	float m_flagWidth;
	float m_flagHeight;

	glm::vec3 m_color;

	float m_animationSpeed;

	bool m_faceRight;

	b2Vec2 m_base;

	void act(Scene *scene);
	Flag(b2World* world);
	virtual ~Flag();

	void construct(b2World *world);
protected:
};

#endif /* FLAG_H_ */
