#include "Mesh.h"

#include <algorithm>

#include <random>


Mesh::Mesh()
{

}

Mesh::Mesh(std::vector<Vertex>& vertices, std::vector<GLuint>& indices)
{
	m_vertices = vertices;
	m_indices = indices;

	//	m_position = glm::vec4(0.0f);

	m_vao.Bind();

	// Generates Vertex Buffer Object and links it to vertices
	VBO vbo(vertices);

	// Links VBO attributes such as coordinates and colors to VAO
	m_vao.LinkAttrib(vbo, 0, 3, GL_FLOAT, sizeof(Vertex), (void*) (0 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 1, 3, GL_FLOAT, sizeof(Vertex), (void*) (3 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 2, 3, GL_FLOAT, sizeof(Vertex), (void*) (6 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 3, 2, GL_FLOAT, sizeof(Vertex), (void*) (9 * sizeof(float)));

	// Unbind all to prevent accidentally modifying them
	m_vao.Unbind();

	//vbo.Delete();
	vbo.Unbind();
}

void Mesh::draw(b2Body* transform, Shader shader, float debugAngle)
{
	m_vao.Bind();

	glm::vec3 position;
	position.x = transform->GetPosition().x;
	position.y = transform->GetPosition().y;

	glm::mat4 model(1.0f);
	model = glm::translate(model, position);
	model = glm::rotate(model, transform->GetAngle()+debugAngle, glm::vec3(0.0f, 0.0f, 1.0f));

	int modelLoc = glGetUniformLocation(shader.ID, "model");
	glUniformMatrix4fv(modelLoc, 1, GL_FALSE, glm::value_ptr(model));

	// Draw the actual mesh
	glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, 0);
	glDrawArrays(GL_TRIANGLES, 0, m_vertices.size());

	//	std::cout<<"drawing this"<<std::endl;
}

Mesh::~Mesh()
{
	//	std::cout << "Mesh destroyed" << std::endl;
	m_vao.Delete();
}

glm::vec3 Mesh::getRandomColor()
{
	float r = (float(rand())/float((RAND_MAX)));
	float g = (float(rand())/float((RAND_MAX)));
	float b = (float(rand())/float((RAND_MAX)));

	return {r, g, b};
}

glmVec3Vec Mesh::triangulate(b2Vec2Vec xy)
{

}

void Mesh::give(std::shared_ptr<b2PolygonShape> polygon)
{
	unsigned int polygonSize = polygon->m_count;

	b2Vec2Vec triangulations;

	for (unsigned int contour=0; contour<polygonSize; contour++)
	{
		Vector2dVector vertices_temp;

		for (unsigned int jjj=0; jjj<polygonSize; jjj++)
		{
			vertices_temp.push_back(Vector2d(polygon->m_vertices[jjj].x, polygon->m_vertices[jjj].y));
		}

		Vector2dVector result;

		Triangulate::Process(vertices_temp, result);

		for (unsigned int pair=0; pair<result.size(); pair++)
		{
			float x = result[pair].GetX();
			float y = result[pair].GetY();

			triangulations.push_back({x, y});
		}
	}

	std::vector<glm::vec2> textureCoordinates;

	float textureScaling = 0.05f;

	float texcoordX = 64*7+16;
	float texcoordY = 48;

//	for (unsigned int corner=0; corner<chainSize; corner++)
//	{
//		float x = chain->m_vertices[corner].x;
//		float y = chain->m_vertices[corner].y;
//		float z = 0.0f;
//
//		positions.push_back({x, y, z});
//
//		float texX = x*textureScaling;
//		float texY = y*textureScaling;
//
//		textureCoordinates.push_back({texX, texY});
//	}

	std::vector<glm::vec3> colors;
	colors.push_back(glm::vec3(1.0f));

	std::vector<glm::vec3> normals;
	normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	std::vector<Vertex> vertices;

	for (int iii=0; iii<triangulations.size(); iii++)
	{
		float x = triangulations[iii].x;
		float y = triangulations[iii].y;
		float z = 0.0f;

		float texX = x*textureScaling;
		float texY = y*textureScaling;

		vertices.push_back({{x, y, z}, colors[0], normals[0], {texX, texY}});
	}

	give(&vertices);
}

void Mesh::give(std::shared_ptr<b2CircleShape> circle)
{
}

void Mesh::give(std::shared_ptr<b2ChainShape> chain)
{
	unsigned int chainSize = chain->m_count;
//	std::cout<<"Chainsize: "<<chainSize<<std::endl;

	/////////////////////////////
	//Triangulate the chain
	//This will be passed straight to opengl, so only one long vector is needed

	b2Vec2Vec triangulations;

	for (unsigned int contour=0; contour<chainSize; contour++)
	{
		Vector2dVector vertices_temp;

		for (unsigned int jjj=0; jjj<chainSize; jjj++)
		{
			vertices_temp.push_back(Vector2d(chain->m_vertices[jjj].x, chain->m_vertices[jjj].y));
		}

		Vector2dVector result;

		Triangulate::Process(vertices_temp, result);

		for (unsigned int coordinates=0; coordinates<result.size(); coordinates++)
		{
			float x = result[coordinates].GetX();
			float y = result[coordinates].GetY();

			triangulations.push_back({x, y});
		}
	}

//	std::cout<<"Triangulation size: "<<triangulations.size()<<std::endl;
	std::cout<<"Triangles in Lunar Surface mesh: "<<triangulations.size()/3<<std::endl;

	/////////////////////////////
	// Determine the renderpath

//	glmVec3Vec positions;
	std::vector<glm::vec2> textureCoordinates;

	float textureScaling = 0.05f;

	float texcoordX = 64*7+16;
	float texcoordY = 48;

//	for (unsigned int corner=0; corner<chainSize; corner++)
//	{
//		float x = chain->m_vertices[corner].x;
//		float y = chain->m_vertices[corner].y;
//		float z = 0.0f;
//
//		positions.push_back({x, y, z});
//
//		float texX = x*textureScaling;
//		float texY = y*textureScaling;
//
//		textureCoordinates.push_back({texX, texY});
//	}

	std::vector<glm::vec3> colors;
	colors.push_back(glm::vec3(1.0f));

	std::vector<glm::vec3> normals;
	normals.push_back(glm::vec3(0.0f, 0.0f, 1.0f));

	std::vector<Vertex> vertices;

	for (int iii=0; iii<triangulations.size(); iii++)
	{
		float x = triangulations[iii].x;
		float y = triangulations[iii].y;
		float z = 0.0f;

//		float texX = textureCoordinates[iii].x;
//		float texY = textureCoordinates[iii].y;

		float texX = x*textureScaling;
		float texY = y*textureScaling;

		vertices.push_back({{x, y, z}, colors[0], normals[0], {texX, texY}});
	}

	give(&vertices);
}

void Mesh::give(std::vector<Vertex>* vertices)
{
//	m_vertices = vertices;
	m_vertices.insert(m_vertices.end(), vertices->begin(), vertices->end());

	m_vao.Bind();

	// Generates Vertex Buffer Object and links it to vertices
	VBO vbo(m_vertices);

	// Links VBO attributes such as coordinates and colors to VAO
	m_vao.LinkAttrib(vbo, 0, 3, GL_FLOAT, sizeof(Vertex), (void*) (0 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 1, 3, GL_FLOAT, sizeof(Vertex), (void*) (3 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 2, 3, GL_FLOAT, sizeof(Vertex), (void*) (6 * sizeof(float)));
	m_vao.LinkAttrib(vbo, 3, 2, GL_FLOAT, sizeof(Vertex), (void*) (9 * sizeof(float)));

	// Unbind all to prevent accidentally modifying them
	m_vao.Unbind();

	//	vbo.Delete();
	vbo.Unbind();
}





























