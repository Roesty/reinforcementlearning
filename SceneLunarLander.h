/*
 * SceneLunarLander.h
 *
 *  Created on: Mar 17, 2022
 *      Author: mooseman
 */

#ifndef SCENELUNARLANDER_H_
#define SCENELUNARLANDER_H_

#include "Scene.h"

#include "LunarSurface.h"
#include "Lander.h"

#include <iostream>
#include <string>

#include <bits/stdc++.h>

class SceneLunarLander: public Scene
{
public:
	virtual void init(SceneManager *sm);
	virtual void cleanup();

	virtual void pause();
	virtual void resume();

	virtual void handleEvents(SceneManager *sm);
	virtual void update(SceneManager *sm);
	virtual void draw(SceneManager *sm);

	virtual void startGui(SceneManager *sm);
	virtual void drawGui(SceneManager *sm);
	virtual void endGui(SceneManager *sm);

	static SceneLunarLander* instance()
	{
		return &m_sceneLunarLander;
	}

	glm::vec3 getToTargetDistance();

protected:
	SceneLunarLander();

private:

	void guiLanderInfo();
	void guiNeuralNetworkInfo();
	void guiSimulationInfo();
	void guiTestControls();
	void guiNeuralNetworkVisualisation();
	void guiEvolutionSimulatorControls();

	ImU32 getColor(int r, int g, int b, int a=0);
	ImU32 getColor(glm::vec3 color);

	const char* m_formatW;
	const char* m_formatS;
	const char* m_formatD;
	const char* m_formatT;
	const char* m_formatScore;
	const char* m_formatInt;

	b2Vec2 m_nextWindowSize;
	b2Vec2 m_nextWindowPosition;
	void addWindowSize();

	static SceneLunarLander m_sceneLunarLander;

	b2Vec2 m_up;
	b2Vec2 m_down;
	b2Vec2 m_left;
	b2Vec2 m_right;
	b2Vec2 getThrustDirection();

	bool m_thrustUp;
	bool m_thrustDown;
	bool m_thrustLeft;
	bool m_thrustRight;

	int m_episodeStepsMax;
	int m_episodeSteps;
	int m_episodeStepsLast;

	float m_episodeDurationMax;
	float m_episodeStart;
	float m_episodeDuration;

	int m_simulatedEpisodeStepsMax;
	int m_simulatedEpisodeSteps;
	std::string m_simulatedEpisodeResult;

	int m_simulationGenerationSize;
	int m_generationsToEvolve;
	std::vector<NeuralNetwork> m_neuralNetworks;
	void evolve(int generations=1);
	std::vector<float> m_generationScores;
	std::vector<float> m_evolutionBestScores;
	NeuralNetwork m_bestNeuralNetwork;

	float m_testStartTime;
	float m_testEndTime;
	float getTestDuration();

	float getDistanceScore();
	void increaseScore();
	float m_testScore;
	float m_evolutionTestScore;

	float m_evolutionStartTime;
	float m_evolutionEndTime;

	glm::vec3 m_lightColor;
	glm::vec3 m_leftFlagColor;
	glm::vec3 m_rightFlagColor;

	ImU32 getColorLerpImGui(ImVec4 low, ImVec4 high, float t);
	float lerp(float low, float high, float t);

	bool m_leftContact;
	bool m_rightContact;

	bool m_hullContact;

	int m_activeCollisions;

	bool isLeftFootLanded();
	bool isRightFootLanded();
	bool isHullColliding();
	b2Vec2 getFootLandingStatus();

	float map(float v, float a0, float a1, float b0, float b1);
	float map(float v, b2Vec2 a, b2Vec2 b);

	void updateLanderInput();

	SDL_Event* m_event;

	std::shared_ptr<LunarSurface> m_lunarSurface;
	std::shared_ptr<Lander> m_lander;

	glm::vec3 m_landerStartingPosition;
	glm::vec3 m_landerTargetPosition;
	float getBaseDistance();

	glm::vec4 m_timeProgressbarColor;

	b2Vec2 m_boundary;

	std::shared_ptr<Flag> m_flagTargetLeft;
	std::shared_ptr<Flag>  m_flagTargetRight;

	std::shared_ptr<Flag> m_flagStartLeft;
	std::shared_ptr<Flag>  m_flagStartRight;

	float runEpisode(SceneManager* sm);

	void resetLanderPosition();
	void resetLanderVelocity();
	void resetLanderAngle();
	void resetLander();
	void resetEpisode();

	void updateTime();

	bool landerLeftBoundary();
	bool timeExpired();
	bool stepLimitReached();
	bool simulationStepLimitReached();
};

#endif /* SCENELUNARLANDER_H_ */
