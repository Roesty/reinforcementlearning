#ifndef MESH_H
#define MESH_H

#include <iostream>

#include "VAO.h"
//#include "EBO.h"

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/rotate_vector.hpp>
#include <glm/gtx/vector_angle.hpp>

#include <Box2D/Box2D.h>

#include "Shader.h"

#include "Triangulate.h"

typedef std::vector<glm::vec3> glmVec3Vec;
typedef std::vector<b2Vec2> b2Vec2Vec;
typedef std::vector<Vertex> vertexVec;

class Mesh
{
public:

	//Give a b2Shape to the Mesh, and the mesh will create the representation
	void give(std::shared_ptr<b2PolygonShape> polygon);
	void give(std::shared_ptr<b2CircleShape> circle);
	void give(std::shared_ptr<b2ChainShape> chain);
	void give(std::vector<Vertex>* vertices);

	std::vector<Vertex> m_vertices;
	std::vector<GLuint> m_indices;

	// Store VAO in public so it can be used in the Draw function
	VAO m_vao;

	//2D vertices used for the physical representation
	b2Vec2Vec m_physicalVertices;

	std::vector<std::shared_ptr<b2PolygonShape>> m_polygonShapes;
	std::vector<std::shared_ptr<b2CircleShape>> m_circleShapes;
	std::vector<std::shared_ptr<b2ChainShape>> m_chainShapes;

	// Initializes the mesh
	Mesh(vertexVec& vertices, std::vector<GLuint>& indices);
	Mesh();
	~Mesh();

	glm::vec3 getRandomColor();
	glmVec3Vec triangulate(b2Vec2Vec xy);

	// Draws the mesh
	void draw(b2Body* transform, Shader shader, float debugAngle);
};

#endif
