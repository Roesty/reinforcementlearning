/*
 * LunarSurface.h
 *
 *  Created on: Mar 21, 2022
 *      Author: mooseman
 */

#ifndef LUNARSURFACE_H_
#define LUNARSURFACE_H_

#include "Entity.h"

#include "Flag.h"

#include <string>

class LunarSurface: public Entity
{
public:
	enum eSensorShapes
	{
		START_GOOD=0,
		START_LEFT,
		START_RIGHT,

		TARGET_GOOD,
		TARGET_LEFT,
		TARGET_RIGHT,

		MAX_SENSOR_SHAPES
	};

	LunarSurface(b2World* world);
	virtual ~LunarSurface();

	float m_width;

	float m_startPlatformWidth;
	float m_targetPlatformWidth;

	float m_startSensorWidth;
	float m_targetSensorWidth;
	float m_sensorHeight;

	glm::vec2 m_startPlatformPosition;
	glm::vec2 m_targetPlatformPosition;

	std::vector<b2Vec2> m_surface;

	std::vector<b2Shape*> m_sensorShapes;
	std::vector<b2Shape*> m_surfaceShapes;

	std::string m_startSensorName;
	std::string m_targetSensorName;
	std::string m_badSensorName;

	void act(Scene* scene);

private:

	void construct(b2World* world);

};

#endif /* LUNARSURFACE_H_ */
