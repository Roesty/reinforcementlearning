/*
 * main.cpp
 *
 *  Created on: Mar 17, 2022
 *      Author: mooseman
 */

#include <iostream>

#include "SceneManager.h"
#include "SceneLunarLander.h"

int main(int ArgCount, char **Args)
{
	std::string applicationName = "Reinforcement Learning";

	std::cout << applicationName << " started" << std::endl;

	SceneManager sceneManager;

	sceneManager.init(applicationName, 1920, 1080);

	// load the intro
	sceneManager.changeState(SceneLunarLander::instance());

	// main loop
	while (sceneManager.running())
	{
		sceneManager.handleEvents();
		sceneManager.update();
		sceneManager.draw();
	}

	// cleanup the engine
	sceneManager.cleanup();

	std::cout << applicationName << " ended" << std::endl;
	return 0;
}
