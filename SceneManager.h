/*
 * LunarLander.h
 *
 *  Created on: Mar 17, 2022
 *      Author: mooseman
 */

#ifndef LUNARLANDER_H_
#define LUNARLANDER_H_

#include <GL/glew.h>

#include <vector>
#include <iostream>

#include <SDL2/SDL.h>
//#include <SDL2/SDL_opengl.h>

#include <Box2D/Box2D.h>

#include <glm/glm.hpp>

class Scene;

class SceneManager
{
public:
	void init(std::string title, int width = 640, int height = 480, int bpp = 0, bool fullscreen = false);

	void cleanup();

	void changeState(Scene *state);
	void pushState(Scene *state);
	void popState();

	void handleEvents();
	void update();
	void draw();

	bool initRandom();
	bool initSDL();
	bool initOpenGL();
	bool initSTBI();
	bool initBox2D();

	void loadSpriteSheet();

	void updateWindowSizes();

	int updateFPS();
	int getFPS();
	float getTime();

	bool running()
	{
		return m_running;
	}
	void quit()
	{
		m_running = false;
	}

	SDL_Window* m_window;
	int m_winWidth;
	int m_winHeight;
	std::string m_winTitle;
	unsigned int m_winFlags;

	SDL_GLContext m_context;

	//OpenGL texture reference
	unsigned int m_spriteSheetTextureID;

	//Time between current frame and last frame
	float m_deltaTime;

	// Time of last frame
	float m_lastFrame;

	std::vector<unsigned int> m_frameCounter;

private:
	// the stack of states
	std::vector<Scene*> states;

	bool m_running;
};

#endif /* LUNARLANDER_H_ */
