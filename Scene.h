/*
 * GameState.h
 *
 *  Created on: Mar 17, 2022
 *      Author: mooseman
 */

#ifndef SCENE_H_
#define SCENE_H_

#include "SceneManager.h"

#include "imgui/imgui.h"
#include "imgui/imgui_impl_sdl.h"
#include "imgui/imgui_impl_opengl3.h"

#include "Camera.h"
#include "NeuralNetwork.h"

class Scene
{
public:

	virtual void init(SceneManager *sm) = 0;
	virtual void cleanup() = 0;

	virtual void pause() = 0;
	virtual void resume() = 0;

	virtual void handleEvents(SceneManager *sm) = 0;
	virtual void update(SceneManager *sm) = 0;
	virtual void draw(SceneManager *sm) = 0;

	virtual void startGui(SceneManager *sm) = 0;
	virtual void drawGui(SceneManager *sm) = 0;
	virtual void endGui(SceneManager *sm) = 0;

	void changeState(SceneManager *sm, Scene *state)
	{
		sm->changeState(state);
	}

	void updateTitle(SceneManager *sm)
	{
		std::string newTitle = sm->m_winTitle + " - " + m_name + ", " + "FPS: " + std::to_string(sm->getFPS());
		SDL_SetWindowTitle(sm->m_window, newTitle.c_str());
	}

	void updateCamera(int winWidth, int winHeight)
	{
		m_camera.updateWidthAndHeight(winWidth, winHeight);
	}

	void printVec3(glm::vec3 vec3, std::string flag="")
	{
		if (flag.size())
		{
			std::cout<<"Printing "<<flag<<" vector"<<std::endl;
		}
		std::cout<<"vec x: "<<vec3.x<<std::endl;
		std::cout<<"    y: "<<vec3.y<<std::endl;
		std::cout<<"    z: "<<vec3.z<<std::endl;
	}

protected:
	Scene()
	{
	}

	SceneManager* m_sm;

	std::shared_ptr<b2Vec2> m_gravity;
	std::shared_ptr<b2World> m_world;
	int m_velocityIterations;
	int m_positionIterations;
	float m_timeStep;

	std::string m_name;

	bool m_isWireframe;
	bool m_isFullscreen;

	Camera m_camera;

	std::shared_ptr<Shader> m_shader;

	glm::vec3 m_clearColor;

};

#endif /* SCENE_H_ */
