/*
 * Lander.h
 *
 *  Created on: Mar 22, 2022
 *      Author: mooseman
 */

#ifndef LANDER_H_
#define LANDER_H_

#include "Entity.h"
#include "NeuralNetwork.h"

class Lander: public Entity
{
public:
	enum eFootShapes
	{
		LEFT=0,
		RIGHT,

		MAX_FOOT_SHAPES
	};

	Lander(b2World* world);
	virtual ~Lander();

	float m_thrusterPower;

	float m_width;
	float m_height;

	float m_sensorSize;

	int m_inNNSize;
	int m_outNNSize;

	bool m_fixedRotation;

	glm::vec3 m_hullColor;
	std::string m_text;

	std::vector<b2Shape*> m_footShapes;

	b2Shape* m_hullShape;
	b2Shape* m_hullSensorShape;

	void act(Scene* scene);

	void construct(b2World* world);

	b2Vec2 getMThrusterActivation() const;
	void setMThrusterActivation(b2Vec2 mThrusterActivation);

	void applyForceToSelf();

private:
	b2Vec2 m_thrusterActivation;
};

#endif /* LANDER_H_ */
