/*
 * Flag.cpp
 *
 *  Created on: Mar 27, 2022
 *      Author: mooseman
 */

#include "Flag.h"

Flag::Flag(b2World* world)
{
	m_poleHeight = 6.0f;
	m_poleWidth = 0.25f;

	m_flagHeight = 2.0f;
	m_flagWidth = 5.0f;

	m_animationSpeed = 1.0f;

	m_faceRight = false;

//	construct(world);
}

Flag::~Flag()
{
	// TODO Auto-generated destructor stub
}

void Flag::act(Scene *scene)
{

}

void Flag::construct(b2World *world)
{
	m_bodyDefinitions.push_back(b2BodyDef());
	m_bodyDefinitions.back().type = b2_staticBody;

	b2Vec2 poleLowerLeft = m_base;
	b2Vec2 poleLowerRight = m_base;
	b2Vec2 poleUpperLeft = m_base;
	b2Vec2 poleUpperRight = m_base;

	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
	m_polygonShapes.back()->SetAsBox(m_poleWidth, m_poleHeight);

	if (m_faceRight)
	{
		for (int corner=0; corner<4; corner++)
		{
			m_polygonShapes.back()->m_vertices[corner].x += m_base.x;
			m_polygonShapes.back()->m_vertices[corner].x += m_poleWidth;

			m_polygonShapes.back()->m_vertices[corner].y += m_base.y;
			m_polygonShapes.back()->m_vertices[corner].y += m_poleHeight;
		}
		poleLowerRight.x += m_poleWidth;
		poleUpperLeft.y += m_poleHeight;

		poleUpperRight.x += m_poleWidth;
		poleUpperRight.y += m_poleHeight;
	}
	else
	{
		for (int corner=0; corner<4; corner++)
		{
			m_polygonShapes.back()->m_vertices[corner].x += m_base.x;
			m_polygonShapes.back()->m_vertices[corner].x += -m_poleWidth;

			m_polygonShapes.back()->m_vertices[corner].y += m_base.y;
			m_polygonShapes.back()->m_vertices[corner].y += m_poleHeight;
		}
		poleLowerLeft.x -= m_poleWidth;
		poleUpperRight.y += m_poleHeight;

		poleUpperLeft.x -= m_poleWidth;
		poleUpperLeft.y += m_poleHeight;
	}

	std::vector<b2Vec2> corners;
	corners.push_back(poleLowerLeft);
	corners.push_back(poleLowerRight);
	corners.push_back(poleUpperRight);
	corners.push_back(poleUpperLeft);

//	b2PolygonShape pole;
//	pole.Set(corners.data(), corners.size());

//	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
//	m_polygonShapes.back()->Set(corners.data(), corners.size());

	//Create the physical representation
	m_physical.reset(new Physical(world, &m_bodyDefinitions[0]));

	//Create the graphical representaion
	m_mesh.reset(new Mesh);

    // Attach the polygons
    for (unsigned int polygon=0; polygon<m_polygonShapes.size(); polygon++)
    {
//    	m_fixtureDefinitions[1].shape = m_polygonShapes[polygon].get();
//    	m_physical->m_body->CreateFixture(&m_fixtureDefinitions[1]);
    	m_mesh->give(m_polygonShapes[polygon]);
    }

}
