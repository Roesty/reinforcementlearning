/*
 * NeuralNetwork.cpp
 *
 *  Created on: Mar 26, 2022
 *      Author: mooseman
 */

#include "NeuralNetwork.h"

NeuralNetwork::NeuralNetwork()
{
	// TODO Auto-generated constructor stub
//	m_in<<0.0f, 2.5f;
}

NeuralNetwork::NeuralNetwork(int in, int out, std::vector<int> middle)
{
	m_in = Eigen::MatrixXd(in, 1);
	m_in.setRandom();

	m_sizes.push_back(in);
	m_sizes.push_back(out);
	m_sizes.insert(m_sizes.begin()+1, middle.begin(), middle.end());

	for (unsigned int layer=1; layer<m_sizes.size(); layer++)
	{
		int rows = m_sizes[layer-1];
		int cols = m_sizes[layer];

		m_layers.push_back(Eigen::MatrixXd(rows, cols));
		m_layers.back().setRandom();
	}

	feedForward();
}

void NeuralNetwork::feedForward()
{
//	std::cout<<"##############"<<std::endl;
//	std::cout<<m_out<<std::endl;
//	std::cout<<"##############"<<std::endl;
//	std::cout<<m_in<<std::endl;
//	std::cout<<"##############"<<std::endl;

	//The first calculation will be the size of the second layer
//	Eigen::MatrixXd values(m_sizes[1], (int)1);

	activationValues.clear();

	Eigen::MatrixXd oldValues = m_in;
	activationValues.push_back(std::vector<float>());

	for (int inputNode=0; inputNode<m_in.rows(); inputNode++)
	{
		activationValues[0].push_back(m_in(inputNode, 0));
	}

	for (unsigned int layer=0; layer<m_layers.size(); layer++)
	{
		int rightSize = m_sizes[layer+1];
		int leftSize = m_sizes[layer];

		activationValues.push_back(std::vector<float>());

		Eigen::MatrixXd newValues(rightSize, 1);

		float v;
		for (int rightNode=0; rightNode<rightSize; rightNode++)
		{
			v = 0.0f;

			for (int leftNode=0; leftNode<leftSize; leftNode++)
			{
				v += m_layers[layer](leftNode, rightNode) * oldValues(leftNode);
			}

			newValues(rightNode, 0) = activate(v);

			activationValues.back().push_back(newValues(rightNode, 0));
		}

		oldValues = newValues;
	}

	m_out = oldValues;

//	float v;
//	for (unsigned int rightNode=0; rightNode<m_out.rows(); rightNode++)
//	{
//		v = 0.0f;
//		for (unsigned int leftNode=0; leftNode<m_in.rows(); leftNode++)
//		{
//			v += m_layers.back()(leftNode, rightNode) * m_in(leftNode, 0);
//		}
//
//		m_out(rightNode, 0) = activate(v);
//	}

//	m_out =
}

float sigmoid(float input)
{
	return 1.0f / (1.0f + (float)exp(input));
}

float flatCurve(float input)
{
	return input / (1 + abs(input));
}

float NeuralNetwork::activate(float input)
{
//	return 1.0f;
//	return sin(input);
	return sigmoid(input);
	return sigmoid(atan(input));
	return atan(input);
	return sigmoid(flatCurve(input));
	return sigmoid(atan(input));
}

void NeuralNetwork::randomise()
{
	for (int layer=0; layer<m_layers.size(); layer++)
	{
		m_layers[layer].setRandom();
	}
}

std::vector<Eigen::MatrixXd> NeuralNetwork::getLayers()
{
	return m_layers;
}

void NeuralNetwork::setLayers(std::vector<Eigen::MatrixXd> layers)
{
	m_sizes.clear();

	m_sizes.push_back(layers[0].rows());

	for (unsigned int layer=0; layer<layers.size(); layer++)
	{
		m_sizes.push_back(layers[layer].cols());
	}
}

float NeuralNetwork::getNodeValue(int layer, int node)
{
	return activationValues[layer][node];
}

void NeuralNetwork::set(std::vector<Eigen::MatrixXd> layers)
{
	m_layers.clear();

	m_layers = layers;

	determineSizes();
}

void NeuralNetwork::set(NeuralNetwork neuralNetwork)
{
	set(neuralNetwork.getLayers());
}

std::vector<Eigen::MatrixXd> NeuralNetwork::get()
{
	return m_layers;
}

void NeuralNetwork::determineSizes()
{
	m_sizes.clear();

	m_sizes.push_back(m_layers[0].rows());

	for (int layer=0; layer<m_layers.size(); layer++)
	{
		m_sizes.push_back(m_layers[layer].cols());
	}
}

void NeuralNetwork::setVariant(std::vector<Eigen::MatrixXd> layers)
{
	//Set the initial layers
	set(layers);

	float randMax = RAND_MAX;
	float v;

	//Add a random value to every synapse
	for (int layer=0; layer<m_layers.size(); layer++)
	{
		for (int row=0; row<m_layers[layer].rows(); row++)
		{
			for (int col=0; col<m_layers[layer].cols(); col++)
			{
				v = rand();

				v /= randMax;

				//random value is now between 0 and 1. Subtract 0.5 to make it centered
				v -= 0.5f;

				m_layers[layer](row, col) += v;
			}
		}
	}
}

void NeuralNetwork::setVariant(NeuralNetwork neuralNetwork)
{
	setVariant(neuralNetwork.getLayers());
}

NeuralNetwork::~NeuralNetwork()
{
	// TODO Auto-generated destructor stub
}






















