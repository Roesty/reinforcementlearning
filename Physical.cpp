/*
 * Physical.cpp
 *
 *  Created on: Mar 21, 2022
 *      Author: mooseman
 */

#include "Physical.h"

Physical::Physical()
{
	// TODO Auto-generated constructor stub
}

Physical::~Physical()
{
	// TODO Auto-generated destructor stub
}

Physical::Physical(b2World *world, b2BodyDef *bodyDef)
{
	m_body = world->CreateBody(bodyDef);
}

glm::vec3 Physical::getPosition()
{
	float x = m_body->GetPosition().x;
	float y = m_body->GetPosition().y;

	return glm::vec3(x, y, 0.0f);
}

void Physical::setPosition(glm::vec3 position)
{
	float x = position.x;
	float y = position.y;

	float angle = m_body->GetAngle();
	m_body->SetTransform(b2Vec2(x, y), angle);
}
