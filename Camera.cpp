#include "Camera.h"

Camera::Camera(int width, int height, glm::vec3 position)
{
	Camera::m_width = width;
	Camera::m_height = height;
	m_position = position;
}

void Camera::updateMatrix(float FOVdeg, float nearPlane, float farPlane)
{
	m_near = nearPlane;
	m_far = farPlane;

	// Initializes matrices since otherwise they will be the null matrix
	glm::mat4 view = glm::mat4(1.0f);
	glm::mat4 projection = glm::mat4(1.0f);

	// Makes camera look in the right direction from the right position
	view = glm::lookAt(m_position, m_position + m_orientation, m_up);

	float ratio = (float)m_width/(float)m_height;
	float halfWidth = m_width/2.0f;

	setMOrthoLeft(-halfWidth);
	setMOrthoRight(halfWidth);

	if (m_projectionMode == PERSPECTIVE)
	{
		// Adds perspective to the scene
		projection = glm::perspective(glm::radians(FOVdeg), ratio, m_near, m_far);
	}

	if (m_projectionMode == OPRTHOGRAPHIC)
	{
		// Adds perspective to the scene
		setMOrthoTop(m_orthoLeft/ratio);
		projection = glm::ortho(m_orthoLeft, m_orthoRight, m_orthoBottom, m_orthoTop, m_orthoNear, m_orthoFar);
	}

	projection = getProjection();

	// Sets new camera matrix
	m_view = projection * view;
}

void Camera::Matrix(Shader& shader, const char* uniform)
{
	// Exports camera matrix
	glUniformMatrix4fv(glGetUniformLocation(shader.ID, uniform), 1, GL_FALSE, glm::value_ptr(m_view));
}

void Camera::setup(int width, int height, glm::vec3 position)
{
	Camera::m_width = width;
	Camera::m_height = height;
	m_position = position;
}

Camera::Camera()
{
	m_fastCameraMultiplier = 5.0f;

	m_near = 0.1f;
	m_far = 1000.0f;
}

void Camera::handleInputs(SDL_Event* event)
{
	//	SDL_PumpEvents();

	const Uint8 *keyboardState = SDL_GetKeyboardState(NULL);

	int mouseX, mouseY;
	Uint32 mouseState = SDL_GetMouseState(&mouseX, &mouseY);

	float movementSpeed = m_speed * m_deltaTime;
	float rotationSpeed = m_sensitivity * m_deltaTime;
	rotationSpeed = m_sensitivity;

	if (keyboardState[SDL_SCANCODE_LSHIFT])
	{
		movementSpeed *= m_fastCameraMultiplier;
	}

	glm::vec3 globalUp(0.0f, 1.0f, 0.0f);

	if (keyboardState[SDL_SCANCODE_UP] || keyboardState[SDL_SCANCODE_W])
	{
		m_position += movementSpeed * m_orientation;
	}

	if (keyboardState[SDL_SCANCODE_DOWN] || keyboardState[SDL_SCANCODE_S])
	{
		m_position -= movementSpeed * m_orientation;
	}

	if (keyboardState[SDL_SCANCODE_RIGHT] || keyboardState[SDL_SCANCODE_D])
	{
		m_position += movementSpeed * glm::normalize(glm::cross(m_orientation, m_up));
	}

	if (keyboardState[SDL_SCANCODE_LEFT] || keyboardState[SDL_SCANCODE_A])
	{
		m_position -= movementSpeed * glm::normalize(glm::cross(m_orientation, m_up));
	}

	if (keyboardState[SDL_SCANCODE_E])
	{
		m_position += movementSpeed * globalUp;
	}

	if (keyboardState[SDL_SCANCODE_Q])
	{
		m_position -= movementSpeed * globalUp;
	}

	//Only rotate the camera if the right mouse button is pressed
	if (mouseState & SDL_BUTTON_RMASK)
	{
		if (event->type == SDL_MOUSEMOTION)
		{
			int relativeX = event->motion.xrel;
			int relativeY = event->motion.yrel;

			m_yaw += relativeX * rotationSpeed;
			m_pitch -= relativeY * rotationSpeed;

//			m_yaw += relativeX;
//			m_pitch -= relativeY;

			while (m_yaw >= 360.0f)
			{
				m_yaw -= 360.0f;
			}

			while (m_yaw < 0.0f)
			{
				m_yaw += 360.0f;
			}

			if (m_pitch > 89.0f)
			{
				m_pitch = 89.0f;
			}

			if (m_pitch < -89.0f)
			{
				m_pitch = -89.0f;
			}

			std::cout<<"Mouse motion "<<event->motion.xrel<<", "<<event->motion.yrel<<std::endl;
			std::cout<<"Yaw, pitch: "<<m_yaw<<", "<<m_pitch<<std::endl;

			//If the camera rotated, update the orientation of the camera
			glm::vec3 direction(0.0f);
			direction.x = cos(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
			direction.y = sin(glm::radians(m_pitch));
			direction.z = sin(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
			setMOrientation(glm::normalize(direction));

			//Reset the event type
			event->type = SDL_FIRSTEVENT;
		}

	}
	if (event->type == SDL_MOUSEWHEEL)
	{
		m_curFOV -= event->wheel.y * m_zoomSensitivity;

		m_curFOV = glm::clamp(m_curFOV, m_minFOV, m_maxFOV);

		//Reset the event type
		event->type = SDL_FIRSTEVENT;
	}

	//		SDL_PumpEvents();
	//
	//		int newMouseX, newMouseY;
	//	 	mouseState = SDL_GetMouseState(&newMouseX, &newMouseY);
	//
	//	 	int deltaX = newMouseX - mouseX;
	//	 	int deltaY = newMouseY - mouseY;
	//
	//	 	std::cout<<deltaX<<", "<<deltaY<<std::endl;

	//    // Handles mouse inputs
	//    if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)
	//    {
	//        // Hides mouse cursor
	//        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
	//
	//        // Prevents camera from jumping on the first click
	//        if (firstClick)
	//        {
	//            glfwSetCursorPos(window, (width / 2), (height / 2));
	//            firstClick = false;
	//        }
	//
	//        // Stores the coordinates of the cursor
	//        double mouseX;
	//        double mouseY;
	//        // Fetches the coordinates of the cursor
	//        glfwGetCursorPos(window, &mouseX, &mouseY);
	//
	//        // Normalizes and shifts the coordinates of the cursor such that they begin in the middle of the screen
	//        // and then "transforms" them into degrees
	//        float rotX = sensitivity * (float)(mouseY - (height / 2)) / m_height;
	//        float rotY = sensitivity * (float)(mouseX - (width / 2)) / width;
	//
	//        // Calculates upcoming vertical change in the Orientation
	//        glm::vec3 newOrientation = glm::rotate(Orientation, glm::radians(-rotX), glm::normalize(glm::cross(Orientation, Up)));
	//
	//        // Decides whether or not the next vertical Orientation is legal or not
	//        if (abs(glm::angle(newOrientation, Up) - glm::radians(90.0f)) <= glm::radians(85.0f))
	//        {
	//            Orientation = newOrientation;
	//        }
	//
	//        // Rotates the Orientation left and right
	//        Orientation = glm::rotate(Orientation, glm::radians(-rotY), Up);
	//
	//        // Sets mouse cursor to the middle of the screen so that it doesn't end up roaming around
	//        glfwSetCursorPos(window, (width / 2), (height / 2));
	//    }
	//    else if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_RELEASE)
	//    {
	//        // Unhides cursor since camera is not looking around anymore
	//        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	//        // Makes sure the next time the camera looks around it doesn't jump
	//        firstClick = true;
	//    }
}

void Camera::handleMouseInput(SDL_Event* event)
{
	int mouseX, mouseY;
	Uint32 mouseState = SDL_GetMouseState(&mouseX, &mouseY);

	float rotationSpeed = m_sensitivity * m_deltaTime;

	//Only rotate the camera if the right mouse button is pressed
	if (mouseState & SDL_BUTTON_RMASK)
	{
		if (event->type == SDL_MOUSEMOTION)
		{
			int relativeX = event->motion.xrel;
			int relativeY = event->motion.yrel;

			m_yaw += relativeX * rotationSpeed;
			m_pitch -= relativeY * rotationSpeed;

			while (m_yaw >= 360.0f)
			{
				m_yaw -= 360.0f;
			}

			while (m_yaw < 0.0f)
			{
				m_yaw += 360.0f;
			}

			if (m_pitch > 89.0f)
			{
				m_pitch = 89.0f;
			}

			if (m_pitch < -89.0f)
			{
				m_pitch = -89.0f;
			}

//			std::cout<<"Mouse motion "<<event->motion.xrel<<", "<<event->motion.yrel<<std::endl;
//			std::cout<<"Yaw, pitch: "<<m_yaw<<", "<<m_pitch<<std::endl;

			//If the camera rotated, update the orientation of the camera
			glm::vec3 direction(0.0f);
			direction.x = cos(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
			direction.y = sin(glm::radians(m_pitch));
			direction.z = sin(glm::radians(m_yaw)) * cos(glm::radians(m_pitch));
			setMOrientation(glm::normalize(direction));
		}
	}

	if (event->type == SDL_MOUSEWHEEL)
	{
		m_curFOV -= event->wheel.y * m_zoomSensitivity;

		m_curFOV = glm::clamp(m_curFOV, m_minFOV, m_maxFOV);
	}
}

void Camera::exportToShader(Shader &shader)
{
	calculateView();
	int viewLoc = glGetUniformLocation(shader.ID, "view");
	glUniformMatrix4fv(viewLoc, 1, GL_FALSE, glm::value_ptr(m_view));
}

void Camera::updateWidthAndHeight(int width, int height)
{
	m_width = width;
	m_height = height;
}

float Camera::ratio()
{
	return (float)m_width/m_height;
}

glm::mat4 Camera::getView()
{
	return glm::lookAt(m_position, m_position + m_orientation, m_up);
}

glm::mat4 Camera::getProjection()
{
	if (m_projectionMode == PERSPECTIVE)
	{
		// Adds perspective to the scene
//		return glm::perspective(glm::radians(m_curFOV), ratio(), m_near, m_far);
		return glm::perspective(glm::radians(m_curFOV), (float)m_width / m_height, m_near, m_far);
	}

	if (m_projectionMode == OPRTHOGRAPHIC)
	{
		// Adds perspective to the scene
		float width = abs(m_orthoRight)+abs(m_orthoLeft);
		float height = width / ratio();

		setMOrthoTop(height-m_orthoBottom);
		return glm::ortho(m_orthoLeft, m_orthoRight, m_orthoBottom, m_orthoTop, m_orthoNear, m_orthoFar);
	}
}

void Camera::handleKeyboardInput()
{
	SDL_PumpEvents();

	const Uint8 *keyboardState = SDL_GetKeyboardState(NULL);

	float movementSpeed = m_speed * m_deltaTime;

	if (keyboardState[SDL_SCANCODE_LSHIFT])
	{
		movementSpeed *= m_fastCameraMultiplier;
	}

	glm::vec3 globalUp(0.0f, 1.0f, 0.0f);
	glm::vec3 movement(0.0f, 0.0f, 0.0f);

	if (keyboardState[SDL_SCANCODE_UP] || keyboardState[SDL_SCANCODE_W])
	{
		movement += movementSpeed * m_orientation;
	}

	if (keyboardState[SDL_SCANCODE_DOWN] || keyboardState[SDL_SCANCODE_S])
	{
		movement -= movementSpeed * m_orientation;
	}

	if (keyboardState[SDL_SCANCODE_RIGHT] || keyboardState[SDL_SCANCODE_D])
	{
		movement += movementSpeed * glm::normalize(glm::cross(m_orientation, m_up));
	}

	if (keyboardState[SDL_SCANCODE_LEFT] || keyboardState[SDL_SCANCODE_A])
	{
		movement -= movementSpeed * glm::normalize(glm::cross(m_orientation, m_up));
	}

	if (keyboardState[SDL_SCANCODE_E])
	{
		movement += movementSpeed * globalUp;
	}

	if (keyboardState[SDL_SCANCODE_Q])
	{
		movement -= movementSpeed * globalUp;
	}

	if (movement != glm::vec3(0.0f, 0.0f, 0.0f))
	{
		m_position += glm::normalize(movement) * movementSpeed;
	}
}

void Camera::calculateView()
{
	m_view = glm::lookAt(m_position, m_position + m_orientation, m_up);
}
