/*
 * NeuralNetwork.h
 *
 *  Created on: Mar 26, 2022
 *      Author: mooseman
 */

#ifndef NEURALNETWORK_H_
#define NEURALNETWORK_H_

//https://www.gormanalysis.com/blog/neural-networks-a-worked-example/

#include <glm/common.hpp>
#include <eigen3/Eigen/Dense>
#include <vector>
#include <iostream>
#include <cmath>

class NeuralNetwork
{
public:
	Eigen::MatrixXd m_in;
	Eigen::MatrixXd m_out;

	Eigen::MatrixXd m_middle;

	std::vector<int> m_sizes;
	std::vector<Eigen::MatrixXd> m_layers;

	std::vector<std::vector<float>> activationValues;

	void set(std::vector<Eigen::MatrixXd> layers);
	void set(NeuralNetwork neuralNetwork);
	std::vector<Eigen::MatrixXd> get();

	void setVariant(std::vector<Eigen::MatrixXd> layers);
	void setVariant(NeuralNetwork neuralNetwork);

	void determineSizes();

	float getNodeValue(int layer, int node);

	void feedForward();
	float activate(float input);

	std::vector<Eigen::MatrixXd> getLayers();
	void setLayers(std::vector<Eigen::MatrixXd> layers);

	void randomise();

	NeuralNetwork();
	NeuralNetwork(int in, int out, std::vector<int> middle);
	virtual ~NeuralNetwork();
};

#endif /* NEURALNETWORK_H_ */
