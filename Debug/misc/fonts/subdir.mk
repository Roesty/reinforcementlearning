################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../misc/fonts/binary_to_compressed_c.cpp 

CPP_DEPS += \
./misc/fonts/binary_to_compressed_c.d 

OBJS += \
./misc/fonts/binary_to_compressed_c.o 


# Each subdirectory must supply rules for building sources it contributes
misc/fonts/%.o: ../misc/fonts/%.cpp misc/fonts/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -IOpenGL -IGLM -IGLEW -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-misc-2f-fonts

clean-misc-2f-fonts:
	-$(RM) ./misc/fonts/binary_to_compressed_c.d ./misc/fonts/binary_to_compressed_c.o

.PHONY: clean-misc-2f-fonts

