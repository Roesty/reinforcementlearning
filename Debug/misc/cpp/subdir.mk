################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../misc/cpp/imgui_stdlib.cpp 

CPP_DEPS += \
./misc/cpp/imgui_stdlib.d 

OBJS += \
./misc/cpp/imgui_stdlib.o 


# Each subdirectory must supply rules for building sources it contributes
misc/cpp/%.o: ../misc/cpp/%.cpp misc/cpp/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -IOpenGL -IGLM -IGLEW -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-misc-2f-cpp

clean-misc-2f-cpp:
	-$(RM) ./misc/cpp/imgui_stdlib.d ./misc/cpp/imgui_stdlib.o

.PHONY: clean-misc-2f-cpp

