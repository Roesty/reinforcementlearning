################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../misc/freetype/imgui_freetype.cpp 

CPP_DEPS += \
./misc/freetype/imgui_freetype.d 

OBJS += \
./misc/freetype/imgui_freetype.o 


# Each subdirectory must supply rules for building sources it contributes
misc/freetype/%.o: ../misc/freetype/%.cpp misc/freetype/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -IOpenGL -IGLM -IGLEW -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-misc-2f-freetype

clean-misc-2f-freetype:
	-$(RM) ./misc/freetype/imgui_freetype.d ./misc/freetype/imgui_freetype.o

.PHONY: clean-misc-2f-freetype

