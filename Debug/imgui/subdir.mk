################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../imgui/imgui.cpp \
../imgui/imgui_demo.cpp \
../imgui/imgui_draw.cpp \
../imgui/imgui_impl_opengl3.cpp \
../imgui/imgui_impl_sdl.cpp \
../imgui/imgui_tables.cpp \
../imgui/imgui_widgets.cpp 

CPP_DEPS += \
./imgui/imgui.d \
./imgui/imgui_demo.d \
./imgui/imgui_draw.d \
./imgui/imgui_impl_opengl3.d \
./imgui/imgui_impl_sdl.d \
./imgui/imgui_tables.d \
./imgui/imgui_widgets.d 

OBJS += \
./imgui/imgui.o \
./imgui/imgui_demo.o \
./imgui/imgui_draw.o \
./imgui/imgui_impl_opengl3.o \
./imgui/imgui_impl_sdl.o \
./imgui/imgui_tables.o \
./imgui/imgui_widgets.o 


# Each subdirectory must supply rules for building sources it contributes
imgui/%.o: ../imgui/%.cpp imgui/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -IOpenGL -IEigen -IGLM -IGLEW -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-imgui

clean-imgui:
	-$(RM) ./imgui/imgui.d ./imgui/imgui.o ./imgui/imgui_demo.d ./imgui/imgui_demo.o ./imgui/imgui_draw.d ./imgui/imgui_draw.o ./imgui/imgui_impl_opengl3.d ./imgui/imgui_impl_opengl3.o ./imgui/imgui_impl_sdl.d ./imgui/imgui_impl_sdl.o ./imgui/imgui_tables.d ./imgui/imgui_tables.o ./imgui/imgui_widgets.d ./imgui/imgui_widgets.o

.PHONY: clean-imgui

