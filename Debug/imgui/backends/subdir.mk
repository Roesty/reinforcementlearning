################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../imgui/backends/imgui_impl_opengl3.cpp \
../imgui/backends/imgui_impl_sdl.cpp 

CPP_DEPS += \
./imgui/backends/imgui_impl_opengl3.d \
./imgui/backends/imgui_impl_sdl.d 

OBJS += \
./imgui/backends/imgui_impl_opengl3.o \
./imgui/backends/imgui_impl_sdl.o 


# Each subdirectory must supply rules for building sources it contributes
imgui/backends/%.o: ../imgui/backends/%.cpp imgui/backends/subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -IOpenGL -IGLM -IGLEW -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean-imgui-2f-backends

clean-imgui-2f-backends:
	-$(RM) ./imgui/backends/imgui_impl_opengl3.d ./imgui/backends/imgui_impl_opengl3.o ./imgui/backends/imgui_impl_sdl.d ./imgui/backends/imgui_impl_sdl.o

.PHONY: clean-imgui-2f-backends

