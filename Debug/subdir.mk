################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Camera.cpp \
../Entity.cpp \
../Flag.cpp \
../Lander.cpp \
../LunarSurface.cpp \
../Mesh.cpp \
../NeuralNetwork.cpp \
../Physical.cpp \
../SceneLunarLander.cpp \
../SceneManager.cpp \
../Shader.cpp \
../Triangulate.cpp \
../VAO.cpp \
../VBO.cpp \
../main.cpp 

CPP_DEPS += \
./Camera.d \
./Entity.d \
./Flag.d \
./Lander.d \
./LunarSurface.d \
./Mesh.d \
./NeuralNetwork.d \
./Physical.d \
./SceneLunarLander.d \
./SceneManager.d \
./Shader.d \
./Triangulate.d \
./VAO.d \
./VBO.d \
./main.d 

OBJS += \
./Camera.o \
./Entity.o \
./Flag.o \
./Lander.o \
./LunarSurface.o \
./Mesh.o \
./NeuralNetwork.o \
./Physical.o \
./SceneLunarLander.o \
./SceneManager.o \
./Shader.o \
./Triangulate.o \
./VAO.o \
./VBO.o \
./main.o 


# Each subdirectory must supply rules for building sources it contributes
%.o: ../%.cpp subdir.mk
	@echo 'Building file: $<'
	@echo 'Invoking: Cross G++ Compiler'
	g++ -IOpenGL -IEigen -IGLM -IGLEW -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


clean: clean--2e-

clean--2e-:
	-$(RM) ./Camera.d ./Camera.o ./Entity.d ./Entity.o ./Flag.d ./Flag.o ./Lander.d ./Lander.o ./LunarSurface.d ./LunarSurface.o ./Mesh.d ./Mesh.o ./NeuralNetwork.d ./NeuralNetwork.o ./Physical.d ./Physical.o ./SceneLunarLander.d ./SceneLunarLander.o ./SceneManager.d ./SceneManager.o ./Shader.d ./Shader.o ./Triangulate.d ./Triangulate.o ./VAO.d ./VAO.o ./VBO.d ./VBO.o ./main.d ./main.o

.PHONY: clean--2e-

