/*
 * Lander.cpp
 *
 *  Created on: Mar 22, 2022
 *      Author: mooseman
 */

#include "Lander.h"

Lander::Lander(b2World* world)
{
	// TODO Auto-generated constructor stub
	m_thrusterPower = 10.0f;

	m_width = 12.0f;
	m_height = 4.0f;

	m_sensorSize = 0.5f;

	m_debugAngle = 0.0f;

	m_inNNSize=6;
	m_outNNSize=4;

	m_fixedRotation=false;

	construct(world);
}

Lander::~Lander()
{
	// TODO Auto-generated destructor stub
}

void Lander::act(Scene* scene)
{
	m_neuralNetwork->feedForward();

	m_thrusterActivation.SetZero();

	m_thrusterActivation.y += glm::max(0.0f, getOutValue(0));
	m_thrusterActivation.y -= glm::max(0.0f, getOutValue(1));

	m_thrusterActivation.x -= glm::max(0.0f, getOutValue(2));
	m_thrusterActivation.x += glm::max(0.0f, getOutValue(3));

	applyForceToSelf();
}

b2Vec2 Lander::getMThrusterActivation() const
{
	return m_thrusterActivation;
}

void Lander::setMThrusterActivation(b2Vec2 mThrusterActivation)
{
	m_thrusterActivation = mThrusterActivation;
}

void Lander::construct(b2World* world)
{
	m_bodyDefinitions.push_back(b2BodyDef());
	m_bodyDefinitions.back().type = b2_dynamicBody;

	m_fixtureDefinitions.push_back(b2FixtureDef());
	m_fixtureDefinitions.back().restitution = 0.2f;
	m_fixtureDefinitions.back().friction = 0.8f;
	m_fixtureDefinitions.back().density = 10.0f;

	m_fixtureDefinitions.push_back(b2FixtureDef());
	m_fixtureDefinitions.back().isSensor = true;
	m_fixtureDefinitions.back().density = 10.0f;

	std::vector<int> fixtureToUse;

	float halfWidth = m_width / 2.0f;
	float halfHeight = m_height / 2.0f;

	std::vector<b2Vec2> hullVertices;
	hullVertices.push_back({-halfWidth, -halfHeight});
	hullVertices.push_back({halfWidth, -halfHeight});
	hullVertices.push_back({halfWidth, 0.0f});
	hullVertices.push_back({halfWidth-1.0f, halfHeight});
	hullVertices.push_back({-halfWidth+1.0f, halfHeight});
	hullVertices.push_back({-halfWidth, 0.0f});

	std::vector<b2Vec2> hullSensorVertices;
	for (unsigned int vertex=0; vertex<hullVertices.size(); vertex++)
	{
		hullSensorVertices.push_back(hullVertices[vertex]);
		if (hullSensorVertices[vertex].x > 0.0f)
		{
			hullSensorVertices[vertex].x += m_sensorSize;
		}
		else
		{
			hullSensorVertices[vertex].x += -m_sensorSize;
		}

		if (hullSensorVertices[vertex].y > 0.0f)
		{
			hullSensorVertices[vertex].y += m_sensorSize;
		}
		else
		{
			hullSensorVertices[vertex].y += -m_sensorSize;
		}
	}

	float legBottomY = -halfHeight / 8.0f * 12.0f;
	float legAncleY = -halfHeight / 8.0f * 11.0f;
	float legTopY = -halfHeight;

	float legBottomFarX = -halfWidth / 12.0f * 2.0f;
	float legBottomNearX = -halfWidth / 12.0f * -2.0f;

	float legAncleFarX = -halfWidth / 12.0f * 1.0f;
	float legAncleNearX = -halfWidth / 12.0f * -1.0f;

	float legTopFarX = -halfWidth / 12.0f * 1.0f;
	float legTopNearX = -halfWidth / 12.0f * -1.0f;

	float legOffset = -halfWidth / 12.0f * 10.0f;

	std::vector<b2Vec2> leg;
	leg.push_back({legAncleFarX, legAncleY});
	leg.push_back({legAncleNearX, legAncleY});
	leg.push_back({legTopNearX, legTopY});
	leg.push_back({legTopFarX, legTopY});

	std::vector<b2Vec2> foot;
	foot.push_back({legBottomFarX, legBottomY});
	foot.push_back({legBottomNearX, legBottomY});
	foot.push_back({legAncleNearX, legAncleY});
	foot.push_back({legAncleFarX, legAncleY});

	std::vector<b2Vec2> leftLegVertices;
	std::vector<b2Vec2> rightLegVertices;

	std::vector<b2Vec2> leftFootVertices;
	std::vector<b2Vec2> rightFootVertices;

	for (int iii=0; iii<leg.size(); iii++)
	{
		leftLegVertices.push_back({leg[iii].x-legOffset, leg[iii].y});
		rightLegVertices.push_back({leg[iii].x+legOffset, leg[iii].y});

		leftFootVertices.push_back({foot[iii].x-legOffset, foot[iii].y});
		rightFootVertices.push_back({foot[iii].x+legOffset, foot[iii].y});
	}

	m_footShapes = std::vector<b2Shape*>(MAX_FOOT_SHAPES);

	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
	m_polygonShapes.back()->Set(hullVertices.data(), hullVertices.size());
	m_hullShape = m_polygonShapes.back().get();
	fixtureToUse.push_back(0);

	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
	m_polygonShapes.back()->Set(hullSensorVertices.data(), hullSensorVertices.size());
	m_hullSensorShape= m_polygonShapes.back().get();
	fixtureToUse.push_back(1);

	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
	m_polygonShapes.back()->Set(leftLegVertices.data(), leftLegVertices.size());
	fixtureToUse.push_back(0);

	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
	m_polygonShapes.back()->Set(rightLegVertices.data(), rightLegVertices.size());
	fixtureToUse.push_back(0);

	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
	m_polygonShapes.back()->Set(leftFootVertices.data(), leftFootVertices.size());
	m_footShapes[LEFT] = m_polygonShapes.back().get();
	fixtureToUse.push_back(0);

	m_polygonShapes.push_back(std::shared_ptr<b2PolygonShape>(new b2PolygonShape));
	m_polygonShapes.back()->Set(rightFootVertices.data(), rightFootVertices.size());
	m_footShapes[RIGHT] = m_polygonShapes.back().get();
	fixtureToUse.push_back(0);

	//Create the physical representation
	m_physical.reset(new Physical(world, &m_bodyDefinitions[0]));
	m_physical->m_body->SetSleepingAllowed(false);
	m_physical->m_body->SetFixedRotation(m_fixedRotation);

	//Create the graphical representaion
	m_mesh.reset(new Mesh);

	// Attach the polygons
	for (unsigned int polygon=0; polygon<m_polygonShapes.size(); polygon++)
	{
		m_fixtureDefinitions[fixtureToUse[polygon]].shape = m_polygonShapes[polygon].get();
		m_physical->m_body->CreateFixture(&m_fixtureDefinitions[fixtureToUse[polygon]]);

		if (fixtureToUse[polygon] == 0)
		{
			m_mesh->give(m_polygonShapes[polygon]);
		}
	}

	// Attach the circles
	for (unsigned int circle=0; circle<m_circleShapes.size(); circle++)
	{
		m_fixtureDefinitions[0].shape = m_circleShapes[circle].get();
		m_physical->m_body->CreateFixture(&m_fixtureDefinitions[0]);
		m_mesh->give(m_circleShapes[circle]);
	}

	// Attach the chains
	for (unsigned int chain=0; chain<m_chainShapes.size(); chain++)
	{
		m_fixtureDefinitions[0].shape = m_chainShapes[chain].get();
		m_physical->m_body->CreateFixture(&m_fixtureDefinitions[0]);
		m_mesh->give(m_chainShapes[chain]);
	}

	//Create the NeuralNetwork
	std::vector<int> middleLayers;
	middleLayers.push_back(6);
	middleLayers.push_back(5);
	middleLayers.push_back(4);
	m_neuralNetwork.reset(new NeuralNetwork(m_inNNSize, m_outNNSize, middleLayers));
}

void Lander::applyForceToSelf()
{
	float force = m_thrusterPower;
	float mass = getMass();
	m_physical->m_body->ApplyForceToCenter(force*mass*getMThrusterActivation(), true);
}
