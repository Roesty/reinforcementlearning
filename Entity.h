/*
 * Entity.h
 *
 *  Created on: Dec 22, 2021
 *      Author: mooseman
 */

#ifndef ENTITY_H_
#define ENTITY_H_

#include <memory>
#include <glm/common.hpp>

#include "Shader.h"
#include "Camera.h"

#include "Mesh.h"
#include "Physical.h"
#include "NeuralNetwork.h"
#include "Scene.h"

class Entity
{
public:

	//The mesh for this entity
	std::shared_ptr<Mesh> m_mesh;

	//The physics of this entity
	std::shared_ptr<Physical> m_physical;

	//The Neural Network
	std::shared_ptr<NeuralNetwork> m_neuralNetwork;

	//The position. This is only used if the entity is not represented by a physical
	//Is never used, but cant be removed because that breaks the program somehow
	glm::vec3 NOT_USED_DO_NOT_REMOVE;

	//The shapes of this entity. These are all added to the fixture when the body is created
	//These are used to determine the physical collisions as well as the graphical representation
	std::vector<std::shared_ptr<b2PolygonShape>> m_polygonShapes;
	std::vector<std::shared_ptr<b2CircleShape>> m_circleShapes;
	std::vector<std::shared_ptr<b2ChainShape>> m_chainShapes;

	//True if this entity is supposed to be drawn
	bool m_draw;

	//debug
	float m_debugAngle;

	//Run this every frame so that the entity can make changes to itself based on the conditions of the game
	virtual void act(Scene* scene) = 0;

	void setInValue(int slot, float value, bool activate=true);
	void setMidValue(int layer, int slot, float value);
	float getOutValue(int slot);
	float getInValue(int slot);

public:

	//Draw this entity
	void draw(Shader shader);

	Entity();
	virtual ~Entity();
	bool isMDraw() const;
	void setMDraw(bool mDraw);

	float randFloat();

	float getMass();
	b2Vec2 getVelocity();
	bool getAwake();

	//	const std::shared_ptr<Mesh>& getMMesh() const;
	//	void setMMesh(const std::shared_ptr<Mesh> mMesh);

	void setPosition(const glm::vec3& position);

	glm::vec3 getPosition()
	{
		if (m_physical.get())
		{
			return m_physical->getPosition();
//			return pos;
		}
		else
		{
//			return m_position;
		}
	}

	void setVelocity(b2Vec2 velocity);
	void setVelocity(glm::vec2 velocity);
	void setVelocity(glm::vec3 velocity);
	void setVelocity(float x, float y);

	void setAngle(float a);

	virtual void construct(b2World* world) = 0;

protected:


	std::vector<b2BodyDef> m_bodyDefinitions;
	std::vector<b2FixtureDef> m_fixtureDefinitions;

};

#endif /* ENTITY_H_ */
