/*
 * LunarLander.cpp
 *
 *  Created on: Mar 17, 2022
 *      Author: mooseman
 */

#include <stb/stb_image.h>

#include "SceneManager.h"
#include "Scene.h"

void SceneManager::init(std::string title, int width, int height, int bpp, bool fullscreen)
{
	m_winTitle = title;
	m_winWidth = width;
	m_winHeight = height;

	initRandom();
	initSDL();
	initOpenGL();
	initSTBI();
	initBox2D();
	loadSpriteSheet();
}

void SceneManager::cleanup()
{
}

void SceneManager::changeState(Scene *state)
{
	// cleanup the current state
	if (!states.empty())
	{
		states.back()->cleanup();
		states.pop_back();
	}

	// store and init the new state
	states.push_back(state);
	states.back()->init(this);
}

void SceneManager::pushState(Scene *state)
{
	// pause current state
	if (!states.empty())
	{
		states.back()->pause();
	}

	// store and init the new state
	states.push_back(state);
	states.back()->init(this);
}

void SceneManager::popState()
{
	// cleanup the current state
	if (!states.empty())
	{
		states.back()->cleanup();
		states.pop_back();
	}

	// resume previous state
	if (!states.empty())
	{
		states.back()->resume();
	}
}

void SceneManager::handleEvents()
{
	states.back()->handleEvents(this);
}

void SceneManager::update()
{
	updateFPS();

	states.back()->update(this);
}

void SceneManager::draw()
{
	updateWindowSizes();
	glViewport(0, 0, m_winWidth, m_winHeight);
	states.back()->updateCamera(m_winWidth, m_winHeight);

	//	std::cout<<"w: "<<m_winWidth<<", h: "<<m_winHeight<<std::endl;

	states.back()->draw(this);
}

bool SceneManager::initRandom()
{
	srand((unsigned int) time(NULL));

	return false;
}

bool SceneManager::initSDL()
{
	m_winFlags = SDL_WINDOW_OPENGL | SDL_WINDOW_RESIZABLE;

	int windowPlacementX = 1920 + (2560-m_winWidth)/2;
	int windowPlacementY = (1440-m_winHeight)/2;

	m_window = SDL_CreateWindow(m_winTitle.c_str(), windowPlacementX, windowPlacementY, m_winWidth, m_winHeight,
		m_winFlags);

	m_context = SDL_GL_CreateContext(m_window);

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);

	return false;
}

bool SceneManager::initOpenGL()
{
	glEnable(GL_DEPTH_TEST);

	glewInit();

	int nrAttributes;
	glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
	std::cout << "Maximum nr of vertex attributes supported: " << nrAttributes << std::endl;

	return false;
}

bool SceneManager::initSTBI()
{
	stbi_set_flip_vertically_on_load(true);
}

bool SceneManager::initBox2D()
{

}

void SceneManager::updateWindowSizes()
{
	SDL_GetWindowSize(m_window, &m_winWidth, &m_winHeight);
}

void SceneManager::loadSpriteSheet()
{
	glGenTextures(1, &m_spriteSheetTextureID);
	glBindTexture(GL_TEXTURE_2D, m_spriteSheetTextureID);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	// load image, create texture and generate mipmaps
	int spritesheetWidth, spritesheetHeight, nrChannels;

	unsigned char *data = stbi_load("res/tex/lunar.jpg", &spritesheetWidth, &spritesheetHeight, &nrChannels, 0);
	if (data)
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, spritesheetWidth, spritesheetHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
	{
		std::cout << "Failed to load texture" << std::endl;
	}
	stbi_image_free(data);

}

int SceneManager::getFPS()
{
	return m_frameCounter.size();
}

int SceneManager::updateFPS()
{
	unsigned int currentTick = SDL_GetTicks();

	m_frameCounter.push_back(currentTick);

	while (currentTick - m_frameCounter[0] >= 1000)
	{
		for (unsigned int iii = 1; iii < m_frameCounter.size(); iii++)
		{
			m_frameCounter[iii - 1] = m_frameCounter[iii];
		}
		m_frameCounter.pop_back();
	}

	float timePassed = (float)currentTick/1000.0f;
	m_deltaTime = timePassed - m_lastFrame;
	m_lastFrame = timePassed;
}

float SceneManager::getTime()
{
	return (float)SDL_GetTicks()/1000.0f;
}
